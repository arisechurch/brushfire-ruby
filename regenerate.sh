#!/bin/bash

rm -rf lib/ docs/ spec/

swagger-codegen generate \
    -i `pwd`/swagger.yaml \
    -l ruby \
    -c `pwd`/config.json \
    -o `pwd`
