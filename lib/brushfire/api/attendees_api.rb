=begin
#Brushfire API: Version 2019-01-17

#Technical details of all available methods are listed below. You can explore this API using the tools at the top of this page. Select your version and provide your app key and click \"Explore\".  For further explanation as well as policies and procedures regarding use of this API, please visit <a href='https://developer.brushfire.com'>https://developer.brushfire.com</a>.  

OpenAPI spec version: 2019-01-17

Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 2.4.4

=end

require 'uri'

module Brushfire
  class AttendeesApi
    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end
    # Associate an external code with a Brushfire attendee for scanning, etc.
    # 
    # @param attendee_id An integer or a GUID that corresponds to a specific attendee
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [AttendeeDetailsOutput]
    def attendees_code(attendee_id, body, opts = {})
      data, _status_code, _headers = attendees_code_with_http_info(attendee_id, body, opts)
      data
    end

    # Associate an external code with a Brushfire attendee for scanning, etc.
    # 
    # @param attendee_id An integer or a GUID that corresponds to a specific attendee
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [Array<(AttendeeDetailsOutput, Fixnum, Hash)>] AttendeeDetailsOutput data, response status code and response headers
    def attendees_code_with_http_info(attendee_id, body, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: AttendeesApi.attendees_code ...'
      end
      # verify the required parameter 'attendee_id' is set
      if @api_client.config.client_side_validation && attendee_id.nil?
        fail ArgumentError, "Missing the required parameter 'attendee_id' when calling AttendeesApi.attendees_code"
      end
      # verify the required parameter 'body' is set
      if @api_client.config.client_side_validation && body.nil?
        fail ArgumentError, "Missing the required parameter 'body' when calling AttendeesApi.attendees_code"
      end
      # resource path
      local_var_path = '/attendees/{attendeeId}/code'.sub('{' + 'attendeeId' + '}', attendee_id.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json', 'text/json', 'application/xml', 'text/xml'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json', 'text/json', 'application/xml', 'text/xml', 'application/x-www-form-urlencoded'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(body)
      auth_names = ['basicAuth']
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'AttendeeDetailsOutput')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: AttendeesApi#attendees_code\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Gets all the available communities for this specific attendee based upon any rules that might be in place
    # 
    # @param attendee_id An integer or a GUID that corresponds to a specific attendee
    # @param [Hash] opts the optional parameters
    # @option opts [String] :access_key The Access Key for the user context
    # @return [Array<AttendeeCommunityOutput>]
    def attendees_communities(attendee_id, opts = {})
      data, _status_code, _headers = attendees_communities_with_http_info(attendee_id, opts)
      data
    end

    # Gets all the available communities for this specific attendee based upon any rules that might be in place
    # 
    # @param attendee_id An integer or a GUID that corresponds to a specific attendee
    # @param [Hash] opts the optional parameters
    # @option opts [String] :access_key The Access Key for the user context
    # @return [Array<(Array<AttendeeCommunityOutput>, Fixnum, Hash)>] Array<AttendeeCommunityOutput> data, response status code and response headers
    def attendees_communities_with_http_info(attendee_id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: AttendeesApi.attendees_communities ...'
      end
      # verify the required parameter 'attendee_id' is set
      if @api_client.config.client_side_validation && attendee_id.nil?
        fail ArgumentError, "Missing the required parameter 'attendee_id' when calling AttendeesApi.attendees_communities"
      end
      # resource path
      local_var_path = '/attendees/{attendeeId}/communities'.sub('{' + 'attendeeId' + '}', attendee_id.to_s)

      # query parameters
      query_params = {}
      query_params[:'accessKey'] = opts[:'access_key'] if !opts[:'access_key'].nil?

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json', 'text/json', 'application/xml', 'text/xml'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = ['basicAuth']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'Array<AttendeeCommunityOutput>')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: AttendeesApi#attendees_communities\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Returns all data for the specified attendee in the same structure as {eventId}/data
    # 
    # @param attendee_id An integer, GUID, or a string that corresponds to a specific attendee
    # @param [Hash] opts the optional parameters
    # @return [Object]
    def attendees_data(attendee_id, opts = {})
      data, _status_code, _headers = attendees_data_with_http_info(attendee_id, opts)
      data
    end

    # Returns all data for the specified attendee in the same structure as {eventId}/data
    # 
    # @param attendee_id An integer, GUID, or a string that corresponds to a specific attendee
    # @param [Hash] opts the optional parameters
    # @return [Array<(Object, Fixnum, Hash)>] Object data, response status code and response headers
    def attendees_data_with_http_info(attendee_id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: AttendeesApi.attendees_data ...'
      end
      # verify the required parameter 'attendee_id' is set
      if @api_client.config.client_side_validation && attendee_id.nil?
        fail ArgumentError, "Missing the required parameter 'attendee_id' when calling AttendeesApi.attendees_data"
      end
      # resource path
      local_var_path = '/attendees/{attendeeId}/data'.sub('{' + 'attendeeId' + '}', attendee_id.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json', 'text/json', 'application/xml', 'text/xml'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = ['basicAuth']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'Object')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: AttendeesApi#attendees_data\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Retrieve all relevant attendee information for the given attendee number
    # 
    # @param attendee_id An integer, GUID, or a string that corresponds to a specific attendee
    # @param [Hash] opts the optional parameters
    # @option opts [String] :event_id An integer or a GUID that corresponds to a specific event (only necessary if sending an attendee code)
    # @option opts [String] :access_key The Access Key for the user context
    # @option opts [Integer] :qr_size The width and height of the QR Code. If blank, no QR will be sent.
    # @return [AttendeeDetailsOutput]
    def attendees_details(attendee_id, opts = {})
      data, _status_code, _headers = attendees_details_with_http_info(attendee_id, opts)
      data
    end

    # Retrieve all relevant attendee information for the given attendee number
    # 
    # @param attendee_id An integer, GUID, or a string that corresponds to a specific attendee
    # @param [Hash] opts the optional parameters
    # @option opts [String] :event_id An integer or a GUID that corresponds to a specific event (only necessary if sending an attendee code)
    # @option opts [String] :access_key The Access Key for the user context
    # @option opts [Integer] :qr_size The width and height of the QR Code. If blank, no QR will be sent.
    # @return [Array<(AttendeeDetailsOutput, Fixnum, Hash)>] AttendeeDetailsOutput data, response status code and response headers
    def attendees_details_with_http_info(attendee_id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: AttendeesApi.attendees_details ...'
      end
      # verify the required parameter 'attendee_id' is set
      if @api_client.config.client_side_validation && attendee_id.nil?
        fail ArgumentError, "Missing the required parameter 'attendee_id' when calling AttendeesApi.attendees_details"
      end
      # resource path
      local_var_path = '/attendees/{attendeeId}'.sub('{' + 'attendeeId' + '}', attendee_id.to_s)

      # query parameters
      query_params = {}
      query_params[:'eventId'] = opts[:'event_id'] if !opts[:'event_id'].nil?
      query_params[:'accessKey'] = opts[:'access_key'] if !opts[:'access_key'].nil?
      query_params[:'qrSize'] = opts[:'qr_size'] if !opts[:'qr_size'].nil?

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json', 'text/json', 'application/xml', 'text/xml'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = ['basicAuth']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'AttendeeDetailsOutput')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: AttendeesApi#attendees_details\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Retrieve fields for an attendee
    # 
    # @param attendee_id An integer or a GUID that corresponds to a specific attendee
    # @param [Hash] opts the optional parameters
    # @option opts [String] :access_key The Access Key for the user context
    # @return [Array<FieldOutput>]
    def attendees_fields(attendee_id, opts = {})
      data, _status_code, _headers = attendees_fields_with_http_info(attendee_id, opts)
      data
    end

    # Retrieve fields for an attendee
    # 
    # @param attendee_id An integer or a GUID that corresponds to a specific attendee
    # @param [Hash] opts the optional parameters
    # @option opts [String] :access_key The Access Key for the user context
    # @return [Array<(Array<FieldOutput>, Fixnum, Hash)>] Array<FieldOutput> data, response status code and response headers
    def attendees_fields_with_http_info(attendee_id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: AttendeesApi.attendees_fields ...'
      end
      # verify the required parameter 'attendee_id' is set
      if @api_client.config.client_side_validation && attendee_id.nil?
        fail ArgumentError, "Missing the required parameter 'attendee_id' when calling AttendeesApi.attendees_fields"
      end
      # resource path
      local_var_path = '/attendees/{attendeeId}/fields'.sub('{' + 'attendeeId' + '}', attendee_id.to_s)

      # query parameters
      query_params = {}
      query_params[:'accessKey'] = opts[:'access_key'] if !opts[:'access_key'].nil?

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json', 'text/json', 'application/xml', 'text/xml'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = ['basicAuth']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'Array<FieldOutput>')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: AttendeesApi#attendees_fields\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Retrieve attendees that match provided email
    # 
    # @param email An email to search across attendee email fields
    # @param [Hash] opts the optional parameters
    # @return [Array<AttendeeHelpdeskOutput>]
    def attendees_helpdesk(email, opts = {})
      data, _status_code, _headers = attendees_helpdesk_with_http_info(email, opts)
      data
    end

    # Retrieve attendees that match provided email
    # 
    # @param email An email to search across attendee email fields
    # @param [Hash] opts the optional parameters
    # @return [Array<(Array<AttendeeHelpdeskOutput>, Fixnum, Hash)>] Array<AttendeeHelpdeskOutput> data, response status code and response headers
    def attendees_helpdesk_with_http_info(email, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: AttendeesApi.attendees_helpdesk ...'
      end
      # verify the required parameter 'email' is set
      if @api_client.config.client_side_validation && email.nil?
        fail ArgumentError, "Missing the required parameter 'email' when calling AttendeesApi.attendees_helpdesk"
      end
      # resource path
      local_var_path = '/attendees/helpdesk/{email}'.sub('{' + 'email' + '}', email.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json', 'text/json', 'application/xml', 'text/xml'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = ['basicAuth']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'Array<AttendeeHelpdeskOutput>')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: AttendeesApi#attendees_helpdesk\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Copy all fields from the source attendee to the specified attendee and link them together
    # 
    # @param attendee_id An integer or a GUID that corresponds to a specific attendee
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [AttendeeDetailsOutput]
    def attendees_link(attendee_id, body, opts = {})
      data, _status_code, _headers = attendees_link_with_http_info(attendee_id, body, opts)
      data
    end

    # Copy all fields from the source attendee to the specified attendee and link them together
    # 
    # @param attendee_id An integer or a GUID that corresponds to a specific attendee
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [Array<(AttendeeDetailsOutput, Fixnum, Hash)>] AttendeeDetailsOutput data, response status code and response headers
    def attendees_link_with_http_info(attendee_id, body, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: AttendeesApi.attendees_link ...'
      end
      # verify the required parameter 'attendee_id' is set
      if @api_client.config.client_side_validation && attendee_id.nil?
        fail ArgumentError, "Missing the required parameter 'attendee_id' when calling AttendeesApi.attendees_link"
      end
      # verify the required parameter 'body' is set
      if @api_client.config.client_side_validation && body.nil?
        fail ArgumentError, "Missing the required parameter 'body' when calling AttendeesApi.attendees_link"
      end
      # resource path
      local_var_path = '/attendees/{attendeeId}/link'.sub('{' + 'attendeeId' + '}', attendee_id.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json', 'text/json', 'application/xml', 'text/xml'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json', 'text/json', 'application/xml', 'text/xml', 'application/x-www-form-urlencoded'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(body)
      auth_names = ['basicAuth']
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'AttendeeDetailsOutput')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: AttendeesApi#attendees_link\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Retrieve the printable resources for a single attendee
    # 
    # @param attendee_id An integer or a GUID that corresponds to a specific attendee
    # @param type A string that is one of \&quot;Ticket\&quot;, \&quot;ETicket\&quot;, or \&quot;Namebadge\&quot;
    # @param [Hash] opts the optional parameters
    # @return [String]
    def attendees_print(attendee_id, type, opts = {})
      data, _status_code, _headers = attendees_print_with_http_info(attendee_id, type, opts)
      data
    end

    # Retrieve the printable resources for a single attendee
    # 
    # @param attendee_id An integer or a GUID that corresponds to a specific attendee
    # @param type A string that is one of \&quot;Ticket\&quot;, \&quot;ETicket\&quot;, or \&quot;Namebadge\&quot;
    # @param [Hash] opts the optional parameters
    # @return [Array<(String, Fixnum, Hash)>] String data, response status code and response headers
    def attendees_print_with_http_info(attendee_id, type, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: AttendeesApi.attendees_print ...'
      end
      # verify the required parameter 'attendee_id' is set
      if @api_client.config.client_side_validation && attendee_id.nil?
        fail ArgumentError, "Missing the required parameter 'attendee_id' when calling AttendeesApi.attendees_print"
      end
      # verify the required parameter 'type' is set
      if @api_client.config.client_side_validation && type.nil?
        fail ArgumentError, "Missing the required parameter 'type' when calling AttendeesApi.attendees_print"
      end
      # verify enum value
      if @api_client.config.client_side_validation && !['Ticket', 'ETicket', 'Receipt', 'MailingLabel', 'WillCall', 'NameBadge', 'Season'].include?(type)
        fail ArgumentError, "invalid value for 'type', must be one of Ticket, ETicket, Receipt, MailingLabel, WillCall, NameBadge, Season"
      end
      # resource path
      local_var_path = '/attendees/{attendeeId}/print'.sub('{' + 'attendeeId' + '}', attendee_id.to_s)

      # query parameters
      query_params = {}
      query_params[:'type'] = type

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json', 'text/json', 'application/xml', 'text/xml'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = ['basicAuth']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'String')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: AttendeesApi#attendees_print\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Retrieve the printable resources for multiple attendees
    # 
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [String]
    def attendees_print_all(body, opts = {})
      data, _status_code, _headers = attendees_print_all_with_http_info(body, opts)
      data
    end

    # Retrieve the printable resources for multiple attendees
    # 
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [Array<(String, Fixnum, Hash)>] String data, response status code and response headers
    def attendees_print_all_with_http_info(body, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: AttendeesApi.attendees_print_all ...'
      end
      # verify the required parameter 'body' is set
      if @api_client.config.client_side_validation && body.nil?
        fail ArgumentError, "Missing the required parameter 'body' when calling AttendeesApi.attendees_print_all"
      end
      # resource path
      local_var_path = '/attendees/print'

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json', 'text/json', 'application/xml', 'text/xml'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json', 'text/json', 'application/xml', 'text/xml', 'application/x-www-form-urlencoded'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(body)
      auth_names = ['basicAuth']
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'String')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: AttendeesApi#attendees_print_all\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Update fields for an attendee
    # 
    # @param attendee_id An integer or a GUID that corresponds to a specific attendee
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [Array<FieldOutput>]
    def attendees_update(attendee_id, body, opts = {})
      data, _status_code, _headers = attendees_update_with_http_info(attendee_id, body, opts)
      data
    end

    # Update fields for an attendee
    # 
    # @param attendee_id An integer or a GUID that corresponds to a specific attendee
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [Array<(Array<FieldOutput>, Fixnum, Hash)>] Array<FieldOutput> data, response status code and response headers
    def attendees_update_with_http_info(attendee_id, body, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: AttendeesApi.attendees_update ...'
      end
      # verify the required parameter 'attendee_id' is set
      if @api_client.config.client_side_validation && attendee_id.nil?
        fail ArgumentError, "Missing the required parameter 'attendee_id' when calling AttendeesApi.attendees_update"
      end
      # verify the required parameter 'body' is set
      if @api_client.config.client_side_validation && body.nil?
        fail ArgumentError, "Missing the required parameter 'body' when calling AttendeesApi.attendees_update"
      end
      # resource path
      local_var_path = '/attendees/{attendeeId}/fields'.sub('{' + 'attendeeId' + '}', attendee_id.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json', 'text/json', 'application/xml', 'text/xml'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json', 'text/json', 'application/xml', 'text/xml', 'application/x-www-form-urlencoded'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(body)
      auth_names = ['basicAuth']
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'Array<FieldOutput>')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: AttendeesApi#attendees_update\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Set or clear the community for the specified attendee (as long as the group's community doesn't override)
    # 
    # @param attendee_id An integer or a GUID that corresponds to a specific attendee
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [AttendeeDetailsOutput]
    def attendees_update_community(attendee_id, body, opts = {})
      data, _status_code, _headers = attendees_update_community_with_http_info(attendee_id, body, opts)
      data
    end

    # Set or clear the community for the specified attendee (as long as the group&#39;s community doesn&#39;t override)
    # 
    # @param attendee_id An integer or a GUID that corresponds to a specific attendee
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [Array<(AttendeeDetailsOutput, Fixnum, Hash)>] AttendeeDetailsOutput data, response status code and response headers
    def attendees_update_community_with_http_info(attendee_id, body, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: AttendeesApi.attendees_update_community ...'
      end
      # verify the required parameter 'attendee_id' is set
      if @api_client.config.client_side_validation && attendee_id.nil?
        fail ArgumentError, "Missing the required parameter 'attendee_id' when calling AttendeesApi.attendees_update_community"
      end
      # verify the required parameter 'body' is set
      if @api_client.config.client_side_validation && body.nil?
        fail ArgumentError, "Missing the required parameter 'body' when calling AttendeesApi.attendees_update_community"
      end
      # resource path
      local_var_path = '/attendees/{attendeeId}/community'.sub('{' + 'attendeeId' + '}', attendee_id.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json', 'text/json', 'application/xml', 'text/xml'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json', 'text/json', 'application/xml', 'text/xml', 'application/x-www-form-urlencoded'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(body)
      auth_names = ['basicAuth']
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'AttendeeDetailsOutput')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: AttendeesApi#attendees_update_community\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Set an attendee registration as a gift
    # 
    # @param attendee_id An integer or a GUID that corresponds to a specific attendee
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [AttendeeUpdateGiftOutput]
    def attendees_update_gift(attendee_id, body, opts = {})
      data, _status_code, _headers = attendees_update_gift_with_http_info(attendee_id, body, opts)
      data
    end

    # Set an attendee registration as a gift
    # 
    # @param attendee_id An integer or a GUID that corresponds to a specific attendee
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [Array<(AttendeeUpdateGiftOutput, Fixnum, Hash)>] AttendeeUpdateGiftOutput data, response status code and response headers
    def attendees_update_gift_with_http_info(attendee_id, body, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: AttendeesApi.attendees_update_gift ...'
      end
      # verify the required parameter 'attendee_id' is set
      if @api_client.config.client_side_validation && attendee_id.nil?
        fail ArgumentError, "Missing the required parameter 'attendee_id' when calling AttendeesApi.attendees_update_gift"
      end
      # verify the required parameter 'body' is set
      if @api_client.config.client_side_validation && body.nil?
        fail ArgumentError, "Missing the required parameter 'body' when calling AttendeesApi.attendees_update_gift"
      end
      # resource path
      local_var_path = '/attendees/{attendeeId}/gift'.sub('{' + 'attendeeId' + '}', attendee_id.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json', 'text/json', 'application/xml', 'text/xml'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json', 'text/json', 'application/xml', 'text/xml', 'application/x-www-form-urlencoded'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(body)
      auth_names = ['basicAuth']
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'AttendeeUpdateGiftOutput')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: AttendeesApi#attendees_update_gift\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
    # Set or clear the group (and the associated community) for the specified attendee
    # 
    # @param attendee_id An integer or a GUID that corresponds to a specific attendee
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [AttendeeDetailsOutput]
    def attendees_update_group(attendee_id, body, opts = {})
      data, _status_code, _headers = attendees_update_group_with_http_info(attendee_id, body, opts)
      data
    end

    # Set or clear the group (and the associated community) for the specified attendee
    # 
    # @param attendee_id An integer or a GUID that corresponds to a specific attendee
    # @param body 
    # @param [Hash] opts the optional parameters
    # @return [Array<(AttendeeDetailsOutput, Fixnum, Hash)>] AttendeeDetailsOutput data, response status code and response headers
    def attendees_update_group_with_http_info(attendee_id, body, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: AttendeesApi.attendees_update_group ...'
      end
      # verify the required parameter 'attendee_id' is set
      if @api_client.config.client_side_validation && attendee_id.nil?
        fail ArgumentError, "Missing the required parameter 'attendee_id' when calling AttendeesApi.attendees_update_group"
      end
      # verify the required parameter 'body' is set
      if @api_client.config.client_side_validation && body.nil?
        fail ArgumentError, "Missing the required parameter 'body' when calling AttendeesApi.attendees_update_group"
      end
      # resource path
      local_var_path = '/attendees/{attendeeId}/group'.sub('{' + 'attendeeId' + '}', attendee_id.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json', 'text/json', 'application/xml', 'text/xml'])
      # HTTP header 'Content-Type'
      header_params['Content-Type'] = @api_client.select_header_content_type(['application/json', 'text/json', 'application/xml', 'text/xml', 'application/x-www-form-urlencoded'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(body)
      auth_names = ['basicAuth']
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'AttendeeDetailsOutput')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: AttendeesApi#attendees_update_group\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
