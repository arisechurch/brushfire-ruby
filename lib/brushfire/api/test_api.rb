=begin
#Brushfire API: Version 2019-01-17

#Technical details of all available methods are listed below. You can explore this API using the tools at the top of this page. Select your version and provide your app key and click \"Explore\".  For further explanation as well as policies and procedures regarding use of this API, please visit <a href='https://developer.brushfire.com'>https://developer.brushfire.com</a>.  

OpenAPI spec version: 2019-01-17

Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 2.4.4

=end

require 'uri'

module Brushfire
  class TestApi
    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end
    # @param event_id 
    # @param [Hash] opts the optional parameters
    # @return [Array<PersonalInfoDataItem>]
    def test_info(event_id, opts = {})
      data, _status_code, _headers = test_info_with_http_info(event_id, opts)
      data
    end

    # @param event_id 
    # @param [Hash] opts the optional parameters
    # @return [Array<(Array<PersonalInfoDataItem>, Fixnum, Hash)>] Array<PersonalInfoDataItem> data, response status code and response headers
    def test_info_with_http_info(event_id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug 'Calling API: TestApi.test_info ...'
      end
      # verify the required parameter 'event_id' is set
      if @api_client.config.client_side_validation && event_id.nil?
        fail ArgumentError, "Missing the required parameter 'event_id' when calling TestApi.test_info"
      end
      # resource path
      local_var_path = '/test/info'

      # query parameters
      query_params = {}
      query_params[:'eventId'] = event_id

      # header parameters
      header_params = {}
      # HTTP header 'Accept' (if needed)
      header_params['Accept'] = @api_client.select_header_accept(['application/json', 'text/json', 'application/xml', 'text/xml'])

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
      auth_names = ['basicAuth']
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'Array<PersonalInfoDataItem>')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: TestApi#test_info\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
