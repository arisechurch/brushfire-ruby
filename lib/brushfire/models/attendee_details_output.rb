=begin
#Brushfire API: Version 2019-01-17

#Technical details of all available methods are listed below. You can explore this API using the tools at the top of this page. Select your version and provide your app key and click \"Explore\".  For further explanation as well as policies and procedures regarding use of this API, please visit <a href='https://developer.brushfire.com'>https://developer.brushfire.com</a>.  

OpenAPI spec version: 2019-01-17

Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 2.4.4

=end

require 'date'

module Brushfire
  class AttendeeDetailsOutput
    attr_accessor :id

    attr_accessor :attendee_number

    attr_accessor :attendee_code

    attr_accessor :event_id

    attr_accessor :event_number

    attr_accessor :status

    attr_accessor :is_completed

    attr_accessor :is_gift

    attr_accessor :is_pre_registered

    attr_accessor :is_printed

    attr_accessor :share_email

    attr_accessor :gift_last_sent_at

    attr_accessor :first_name

    attr_accessor :last_name

    attr_accessor :phone

    attr_accessor :email

    attr_accessor :street1

    attr_accessor :street2

    attr_accessor :city

    attr_accessor :region

    attr_accessor :country

    attr_accessor :postal_code

    attr_accessor :type_id

    attr_accessor :type_name

    attr_accessor :section_name

    attr_accessor :row_name

    attr_accessor :seat_label

    attr_accessor :wheelchair

    attr_accessor :limited_view

    attr_accessor :amount

    attr_accessor :seat_info

    attr_accessor :community_id

    attr_accessor :community_name

    attr_accessor :community_partition

    attr_accessor :group_id

    attr_accessor :group_name

    attr_accessor :group_email

    attr_accessor :fields

    attr_accessor :order_fields

    attr_accessor :group_fields

    attr_accessor :checkins

    attr_accessor :qr_code

    attr_accessor :is_anonymized

    # Attribute mapping from ruby-style variable name to JSON key.
    def self.attribute_map
      {
        :'id' => :'Id',
        :'attendee_number' => :'AttendeeNumber',
        :'attendee_code' => :'AttendeeCode',
        :'event_id' => :'EventId',
        :'event_number' => :'EventNumber',
        :'status' => :'Status',
        :'is_completed' => :'IsCompleted',
        :'is_gift' => :'IsGift',
        :'is_pre_registered' => :'IsPreRegistered',
        :'is_printed' => :'IsPrinted',
        :'share_email' => :'ShareEmail',
        :'gift_last_sent_at' => :'GiftLastSentAt',
        :'first_name' => :'FirstName',
        :'last_name' => :'LastName',
        :'phone' => :'Phone',
        :'email' => :'Email',
        :'street1' => :'Street1',
        :'street2' => :'Street2',
        :'city' => :'City',
        :'region' => :'Region',
        :'country' => :'Country',
        :'postal_code' => :'PostalCode',
        :'type_id' => :'TypeId',
        :'type_name' => :'TypeName',
        :'section_name' => :'SectionName',
        :'row_name' => :'RowName',
        :'seat_label' => :'SeatLabel',
        :'wheelchair' => :'Wheelchair',
        :'limited_view' => :'LimitedView',
        :'amount' => :'Amount',
        :'seat_info' => :'SeatInfo',
        :'community_id' => :'CommunityId',
        :'community_name' => :'CommunityName',
        :'community_partition' => :'CommunityPartition',
        :'group_id' => :'GroupId',
        :'group_name' => :'GroupName',
        :'group_email' => :'GroupEmail',
        :'fields' => :'Fields',
        :'order_fields' => :'OrderFields',
        :'group_fields' => :'GroupFields',
        :'checkins' => :'Checkins',
        :'qr_code' => :'QRCode',
        :'is_anonymized' => :'IsAnonymized'
      }
    end

    # Attribute type mapping.
    def self.swagger_types
      {
        :'id' => :'String',
        :'attendee_number' => :'Integer',
        :'attendee_code' => :'String',
        :'event_id' => :'String',
        :'event_number' => :'Integer',
        :'status' => :'String',
        :'is_completed' => :'BOOLEAN',
        :'is_gift' => :'BOOLEAN',
        :'is_pre_registered' => :'BOOLEAN',
        :'is_printed' => :'BOOLEAN',
        :'share_email' => :'String',
        :'gift_last_sent_at' => :'DateTime',
        :'first_name' => :'String',
        :'last_name' => :'String',
        :'phone' => :'String',
        :'email' => :'String',
        :'street1' => :'String',
        :'street2' => :'String',
        :'city' => :'String',
        :'region' => :'String',
        :'country' => :'String',
        :'postal_code' => :'String',
        :'type_id' => :'String',
        :'type_name' => :'String',
        :'section_name' => :'String',
        :'row_name' => :'String',
        :'seat_label' => :'String',
        :'wheelchair' => :'BOOLEAN',
        :'limited_view' => :'BOOLEAN',
        :'amount' => :'Float',
        :'seat_info' => :'String',
        :'community_id' => :'String',
        :'community_name' => :'String',
        :'community_partition' => :'String',
        :'group_id' => :'String',
        :'group_name' => :'String',
        :'group_email' => :'String',
        :'fields' => :'Array<FieldDisplayOutput>',
        :'order_fields' => :'Array<FieldDisplayOutput>',
        :'group_fields' => :'Array<FieldDisplayOutput>',
        :'checkins' => :'Array<AttendeeCheckinOutput>',
        :'qr_code' => :'String',
        :'is_anonymized' => :'BOOLEAN'
      }
    end

    # Initializes the object
    # @param [Hash] attributes Model attributes in the form of hash
    def initialize(attributes = {})
      return unless attributes.is_a?(Hash)

      # convert string to symbol for hash key
      attributes = attributes.each_with_object({}) { |(k, v), h| h[k.to_sym] = v }

      if attributes.has_key?(:'Id')
        self.id = attributes[:'Id']
      end

      if attributes.has_key?(:'AttendeeNumber')
        self.attendee_number = attributes[:'AttendeeNumber']
      end

      if attributes.has_key?(:'AttendeeCode')
        self.attendee_code = attributes[:'AttendeeCode']
      end

      if attributes.has_key?(:'EventId')
        self.event_id = attributes[:'EventId']
      end

      if attributes.has_key?(:'EventNumber')
        self.event_number = attributes[:'EventNumber']
      end

      if attributes.has_key?(:'Status')
        self.status = attributes[:'Status']
      end

      if attributes.has_key?(:'IsCompleted')
        self.is_completed = attributes[:'IsCompleted']
      end

      if attributes.has_key?(:'IsGift')
        self.is_gift = attributes[:'IsGift']
      end

      if attributes.has_key?(:'IsPreRegistered')
        self.is_pre_registered = attributes[:'IsPreRegistered']
      end

      if attributes.has_key?(:'IsPrinted')
        self.is_printed = attributes[:'IsPrinted']
      end

      if attributes.has_key?(:'ShareEmail')
        self.share_email = attributes[:'ShareEmail']
      end

      if attributes.has_key?(:'GiftLastSentAt')
        self.gift_last_sent_at = attributes[:'GiftLastSentAt']
      end

      if attributes.has_key?(:'FirstName')
        self.first_name = attributes[:'FirstName']
      end

      if attributes.has_key?(:'LastName')
        self.last_name = attributes[:'LastName']
      end

      if attributes.has_key?(:'Phone')
        self.phone = attributes[:'Phone']
      end

      if attributes.has_key?(:'Email')
        self.email = attributes[:'Email']
      end

      if attributes.has_key?(:'Street1')
        self.street1 = attributes[:'Street1']
      end

      if attributes.has_key?(:'Street2')
        self.street2 = attributes[:'Street2']
      end

      if attributes.has_key?(:'City')
        self.city = attributes[:'City']
      end

      if attributes.has_key?(:'Region')
        self.region = attributes[:'Region']
      end

      if attributes.has_key?(:'Country')
        self.country = attributes[:'Country']
      end

      if attributes.has_key?(:'PostalCode')
        self.postal_code = attributes[:'PostalCode']
      end

      if attributes.has_key?(:'TypeId')
        self.type_id = attributes[:'TypeId']
      end

      if attributes.has_key?(:'TypeName')
        self.type_name = attributes[:'TypeName']
      end

      if attributes.has_key?(:'SectionName')
        self.section_name = attributes[:'SectionName']
      end

      if attributes.has_key?(:'RowName')
        self.row_name = attributes[:'RowName']
      end

      if attributes.has_key?(:'SeatLabel')
        self.seat_label = attributes[:'SeatLabel']
      end

      if attributes.has_key?(:'Wheelchair')
        self.wheelchair = attributes[:'Wheelchair']
      end

      if attributes.has_key?(:'LimitedView')
        self.limited_view = attributes[:'LimitedView']
      end

      if attributes.has_key?(:'Amount')
        self.amount = attributes[:'Amount']
      end

      if attributes.has_key?(:'SeatInfo')
        self.seat_info = attributes[:'SeatInfo']
      end

      if attributes.has_key?(:'CommunityId')
        self.community_id = attributes[:'CommunityId']
      end

      if attributes.has_key?(:'CommunityName')
        self.community_name = attributes[:'CommunityName']
      end

      if attributes.has_key?(:'CommunityPartition')
        self.community_partition = attributes[:'CommunityPartition']
      end

      if attributes.has_key?(:'GroupId')
        self.group_id = attributes[:'GroupId']
      end

      if attributes.has_key?(:'GroupName')
        self.group_name = attributes[:'GroupName']
      end

      if attributes.has_key?(:'GroupEmail')
        self.group_email = attributes[:'GroupEmail']
      end

      if attributes.has_key?(:'Fields')
        if (value = attributes[:'Fields']).is_a?(Array)
          self.fields = value
        end
      end

      if attributes.has_key?(:'OrderFields')
        if (value = attributes[:'OrderFields']).is_a?(Array)
          self.order_fields = value
        end
      end

      if attributes.has_key?(:'GroupFields')
        if (value = attributes[:'GroupFields']).is_a?(Array)
          self.group_fields = value
        end
      end

      if attributes.has_key?(:'Checkins')
        if (value = attributes[:'Checkins']).is_a?(Array)
          self.checkins = value
        end
      end

      if attributes.has_key?(:'QRCode')
        self.qr_code = attributes[:'QRCode']
      end

      if attributes.has_key?(:'IsAnonymized')
        self.is_anonymized = attributes[:'IsAnonymized']
      end
    end

    # Show invalid properties with the reasons. Usually used together with valid?
    # @return Array for valid properties with the reasons
    def list_invalid_properties
      invalid_properties = Array.new
      invalid_properties
    end

    # Check to see if the all the properties in the model are valid
    # @return true if the model is valid
    def valid?
      true
    end

    # Checks equality by comparing each attribute.
    # @param [Object] Object to be compared
    def ==(o)
      return true if self.equal?(o)
      self.class == o.class &&
          id == o.id &&
          attendee_number == o.attendee_number &&
          attendee_code == o.attendee_code &&
          event_id == o.event_id &&
          event_number == o.event_number &&
          status == o.status &&
          is_completed == o.is_completed &&
          is_gift == o.is_gift &&
          is_pre_registered == o.is_pre_registered &&
          is_printed == o.is_printed &&
          share_email == o.share_email &&
          gift_last_sent_at == o.gift_last_sent_at &&
          first_name == o.first_name &&
          last_name == o.last_name &&
          phone == o.phone &&
          email == o.email &&
          street1 == o.street1 &&
          street2 == o.street2 &&
          city == o.city &&
          region == o.region &&
          country == o.country &&
          postal_code == o.postal_code &&
          type_id == o.type_id &&
          type_name == o.type_name &&
          section_name == o.section_name &&
          row_name == o.row_name &&
          seat_label == o.seat_label &&
          wheelchair == o.wheelchair &&
          limited_view == o.limited_view &&
          amount == o.amount &&
          seat_info == o.seat_info &&
          community_id == o.community_id &&
          community_name == o.community_name &&
          community_partition == o.community_partition &&
          group_id == o.group_id &&
          group_name == o.group_name &&
          group_email == o.group_email &&
          fields == o.fields &&
          order_fields == o.order_fields &&
          group_fields == o.group_fields &&
          checkins == o.checkins &&
          qr_code == o.qr_code &&
          is_anonymized == o.is_anonymized
    end

    # @see the `==` method
    # @param [Object] Object to be compared
    def eql?(o)
      self == o
    end

    # Calculates hash code according to all attributes.
    # @return [Fixnum] Hash code
    def hash
      [id, attendee_number, attendee_code, event_id, event_number, status, is_completed, is_gift, is_pre_registered, is_printed, share_email, gift_last_sent_at, first_name, last_name, phone, email, street1, street2, city, region, country, postal_code, type_id, type_name, section_name, row_name, seat_label, wheelchair, limited_view, amount, seat_info, community_id, community_name, community_partition, group_id, group_name, group_email, fields, order_fields, group_fields, checkins, qr_code, is_anonymized].hash
    end

    # Builds the object from hash
    # @param [Hash] attributes Model attributes in the form of hash
    # @return [Object] Returns the model itself
    def build_from_hash(attributes)
      return nil unless attributes.is_a?(Hash)
      self.class.swagger_types.each_pair do |key, type|
        if type =~ /\AArray<(.*)>/i
          # check to ensure the input is an array given that the the attribute
          # is documented as an array but the input is not
          if attributes[self.class.attribute_map[key]].is_a?(Array)
            self.send("#{key}=", attributes[self.class.attribute_map[key]].map { |v| _deserialize($1, v) })
          end
        elsif !attributes[self.class.attribute_map[key]].nil?
          self.send("#{key}=", _deserialize(type, attributes[self.class.attribute_map[key]]))
        end # or else data not found in attributes(hash), not an issue as the data can be optional
      end

      self
    end

    # Deserializes the data based on type
    # @param string type Data type
    # @param string value Value to be deserialized
    # @return [Object] Deserialized data
    def _deserialize(type, value)
      case type.to_sym
      when :DateTime
        DateTime.parse(value)
      when :Date
        Date.parse(value)
      when :String
        value.to_s
      when :Integer
        value.to_i
      when :Float
        value.to_f
      when :BOOLEAN
        if value.to_s =~ /\A(true|t|yes|y|1)\z/i
          true
        else
          false
        end
      when :Object
        # generic object (usually a Hash), return directly
        value
      when /\AArray<(?<inner_type>.+)>\z/
        inner_type = Regexp.last_match[:inner_type]
        value.map { |v| _deserialize(inner_type, v) }
      when /\AHash<(?<k_type>.+?), (?<v_type>.+)>\z/
        k_type = Regexp.last_match[:k_type]
        v_type = Regexp.last_match[:v_type]
        {}.tap do |hash|
          value.each do |k, v|
            hash[_deserialize(k_type, k)] = _deserialize(v_type, v)
          end
        end
      else # model
        temp_model = Brushfire.const_get(type).new
        temp_model.build_from_hash(value)
      end
    end

    # Returns the string representation of the object
    # @return [String] String presentation of the object
    def to_s
      to_hash.to_s
    end

    # to_body is an alias to to_hash (backward compatibility)
    # @return [Hash] Returns the object in the form of hash
    def to_body
      to_hash
    end

    # Returns the object in the form of hash
    # @return [Hash] Returns the object in the form of hash
    def to_hash
      hash = {}
      self.class.attribute_map.each_pair do |attr, param|
        value = self.send(attr)
        next if value.nil?
        hash[param] = _to_hash(value)
      end
      hash
    end

    # Outputs non-array value in the form of hash
    # For object, use to_hash. Otherwise, just return the value
    # @param [Object] value Any valid value
    # @return [Hash] Returns the value in the form of hash
    def _to_hash(value)
      if value.is_a?(Array)
        value.compact.map { |v| _to_hash(v) }
      elsif value.is_a?(Hash)
        {}.tap do |hash|
          value.each { |k, v| hash[k] = _to_hash(v) }
        end
      elsif value.respond_to? :to_hash
        value.to_hash
      else
        value
      end
    end
  end
end
