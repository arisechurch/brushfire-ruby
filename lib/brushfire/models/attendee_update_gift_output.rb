=begin
#Brushfire API: Version 2019-01-17

#Technical details of all available methods are listed below. You can explore this API using the tools at the top of this page. Select your version and provide your app key and click \"Explore\".  For further explanation as well as policies and procedures regarding use of this API, please visit <a href='https://developer.brushfire.com'>https://developer.brushfire.com</a>.  

OpenAPI spec version: 2019-01-17

Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 2.4.4

=end

require 'date'

module Brushfire
  class AttendeeUpdateGiftOutput
    attr_accessor :id

    attr_accessor :attendee_number

    attr_accessor :event_id

    attr_accessor :attendee_type_id

    attr_accessor :attendee_code

    attr_accessor :group_id

    attr_accessor :is_completed

    attr_accessor :order_id

    attr_accessor :current_order_id

    attr_accessor :original_order_id

    attr_accessor :cart_id

    attr_accessor :is_gift

    attr_accessor :is_pre_registered

    attr_accessor :recipient_email

    # Attribute mapping from ruby-style variable name to JSON key.
    def self.attribute_map
      {
        :'id' => :'Id',
        :'attendee_number' => :'AttendeeNumber',
        :'event_id' => :'EventId',
        :'attendee_type_id' => :'AttendeeTypeId',
        :'attendee_code' => :'AttendeeCode',
        :'group_id' => :'GroupId',
        :'is_completed' => :'IsCompleted',
        :'order_id' => :'OrderId',
        :'current_order_id' => :'CurrentOrderId',
        :'original_order_id' => :'OriginalOrderId',
        :'cart_id' => :'CartId',
        :'is_gift' => :'IsGift',
        :'is_pre_registered' => :'IsPreRegistered',
        :'recipient_email' => :'RecipientEmail'
      }
    end

    # Attribute type mapping.
    def self.swagger_types
      {
        :'id' => :'String',
        :'attendee_number' => :'Integer',
        :'event_id' => :'String',
        :'attendee_type_id' => :'String',
        :'attendee_code' => :'String',
        :'group_id' => :'String',
        :'is_completed' => :'BOOLEAN',
        :'order_id' => :'String',
        :'current_order_id' => :'String',
        :'original_order_id' => :'String',
        :'cart_id' => :'String',
        :'is_gift' => :'BOOLEAN',
        :'is_pre_registered' => :'BOOLEAN',
        :'recipient_email' => :'String'
      }
    end

    # Initializes the object
    # @param [Hash] attributes Model attributes in the form of hash
    def initialize(attributes = {})
      return unless attributes.is_a?(Hash)

      # convert string to symbol for hash key
      attributes = attributes.each_with_object({}) { |(k, v), h| h[k.to_sym] = v }

      if attributes.has_key?(:'Id')
        self.id = attributes[:'Id']
      end

      if attributes.has_key?(:'AttendeeNumber')
        self.attendee_number = attributes[:'AttendeeNumber']
      end

      if attributes.has_key?(:'EventId')
        self.event_id = attributes[:'EventId']
      end

      if attributes.has_key?(:'AttendeeTypeId')
        self.attendee_type_id = attributes[:'AttendeeTypeId']
      end

      if attributes.has_key?(:'AttendeeCode')
        self.attendee_code = attributes[:'AttendeeCode']
      end

      if attributes.has_key?(:'GroupId')
        self.group_id = attributes[:'GroupId']
      end

      if attributes.has_key?(:'IsCompleted')
        self.is_completed = attributes[:'IsCompleted']
      end

      if attributes.has_key?(:'OrderId')
        self.order_id = attributes[:'OrderId']
      end

      if attributes.has_key?(:'CurrentOrderId')
        self.current_order_id = attributes[:'CurrentOrderId']
      end

      if attributes.has_key?(:'OriginalOrderId')
        self.original_order_id = attributes[:'OriginalOrderId']
      end

      if attributes.has_key?(:'CartId')
        self.cart_id = attributes[:'CartId']
      end

      if attributes.has_key?(:'IsGift')
        self.is_gift = attributes[:'IsGift']
      end

      if attributes.has_key?(:'IsPreRegistered')
        self.is_pre_registered = attributes[:'IsPreRegistered']
      end

      if attributes.has_key?(:'RecipientEmail')
        self.recipient_email = attributes[:'RecipientEmail']
      end
    end

    # Show invalid properties with the reasons. Usually used together with valid?
    # @return Array for valid properties with the reasons
    def list_invalid_properties
      invalid_properties = Array.new
      invalid_properties
    end

    # Check to see if the all the properties in the model are valid
    # @return true if the model is valid
    def valid?
      true
    end

    # Checks equality by comparing each attribute.
    # @param [Object] Object to be compared
    def ==(o)
      return true if self.equal?(o)
      self.class == o.class &&
          id == o.id &&
          attendee_number == o.attendee_number &&
          event_id == o.event_id &&
          attendee_type_id == o.attendee_type_id &&
          attendee_code == o.attendee_code &&
          group_id == o.group_id &&
          is_completed == o.is_completed &&
          order_id == o.order_id &&
          current_order_id == o.current_order_id &&
          original_order_id == o.original_order_id &&
          cart_id == o.cart_id &&
          is_gift == o.is_gift &&
          is_pre_registered == o.is_pre_registered &&
          recipient_email == o.recipient_email
    end

    # @see the `==` method
    # @param [Object] Object to be compared
    def eql?(o)
      self == o
    end

    # Calculates hash code according to all attributes.
    # @return [Fixnum] Hash code
    def hash
      [id, attendee_number, event_id, attendee_type_id, attendee_code, group_id, is_completed, order_id, current_order_id, original_order_id, cart_id, is_gift, is_pre_registered, recipient_email].hash
    end

    # Builds the object from hash
    # @param [Hash] attributes Model attributes in the form of hash
    # @return [Object] Returns the model itself
    def build_from_hash(attributes)
      return nil unless attributes.is_a?(Hash)
      self.class.swagger_types.each_pair do |key, type|
        if type =~ /\AArray<(.*)>/i
          # check to ensure the input is an array given that the the attribute
          # is documented as an array but the input is not
          if attributes[self.class.attribute_map[key]].is_a?(Array)
            self.send("#{key}=", attributes[self.class.attribute_map[key]].map { |v| _deserialize($1, v) })
          end
        elsif !attributes[self.class.attribute_map[key]].nil?
          self.send("#{key}=", _deserialize(type, attributes[self.class.attribute_map[key]]))
        end # or else data not found in attributes(hash), not an issue as the data can be optional
      end

      self
    end

    # Deserializes the data based on type
    # @param string type Data type
    # @param string value Value to be deserialized
    # @return [Object] Deserialized data
    def _deserialize(type, value)
      case type.to_sym
      when :DateTime
        DateTime.parse(value)
      when :Date
        Date.parse(value)
      when :String
        value.to_s
      when :Integer
        value.to_i
      when :Float
        value.to_f
      when :BOOLEAN
        if value.to_s =~ /\A(true|t|yes|y|1)\z/i
          true
        else
          false
        end
      when :Object
        # generic object (usually a Hash), return directly
        value
      when /\AArray<(?<inner_type>.+)>\z/
        inner_type = Regexp.last_match[:inner_type]
        value.map { |v| _deserialize(inner_type, v) }
      when /\AHash<(?<k_type>.+?), (?<v_type>.+)>\z/
        k_type = Regexp.last_match[:k_type]
        v_type = Regexp.last_match[:v_type]
        {}.tap do |hash|
          value.each do |k, v|
            hash[_deserialize(k_type, k)] = _deserialize(v_type, v)
          end
        end
      else # model
        temp_model = Brushfire.const_get(type).new
        temp_model.build_from_hash(value)
      end
    end

    # Returns the string representation of the object
    # @return [String] String presentation of the object
    def to_s
      to_hash.to_s
    end

    # to_body is an alias to to_hash (backward compatibility)
    # @return [Hash] Returns the object in the form of hash
    def to_body
      to_hash
    end

    # Returns the object in the form of hash
    # @return [Hash] Returns the object in the form of hash
    def to_hash
      hash = {}
      self.class.attribute_map.each_pair do |attr, param|
        value = self.send(attr)
        next if value.nil?
        hash[param] = _to_hash(value)
      end
      hash
    end

    # Outputs non-array value in the form of hash
    # For object, use to_hash. Otherwise, just return the value
    # @param [Object] value Any valid value
    # @return [Hash] Returns the value in the form of hash
    def _to_hash(value)
      if value.is_a?(Array)
        value.compact.map { |v| _to_hash(v) }
      elsif value.is_a?(Hash)
        {}.tap do |hash|
          value.each { |k, v| hash[k] = _to_hash(v) }
        end
      elsif value.respond_to? :to_hash
        value.to_hash
      else
        value
      end
    end
  end
end
