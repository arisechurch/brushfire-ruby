=begin
#Brushfire API: Version 2019-01-17

#Technical details of all available methods are listed below. You can explore this API using the tools at the top of this page. Select your version and provide your app key and click \"Explore\".  For further explanation as well as policies and procedures regarding use of this API, please visit <a href='https://developer.brushfire.com'>https://developer.brushfire.com</a>.  

OpenAPI spec version: 2019-01-17

Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 2.4.4

=end

require 'spec_helper'
require 'json'
require 'date'

# Unit tests for Brushfire::FieldOptionFullOutput
# Automatically generated by swagger-codegen (github.com/swagger-api/swagger-codegen)
# Please update as you see appropriate
describe 'FieldOptionFullOutput' do
  before do
    # run before each test
    @instance = Brushfire::FieldOptionFullOutput.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of FieldOptionFullOutput' do
    it 'should create an instance of FieldOptionFullOutput' do
      expect(@instance).to be_instance_of(Brushfire::FieldOptionFullOutput)
    end
  end
  describe 'test attribute "id"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "label"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "capacity"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "quantity"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "has_unit_price"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "unit_price"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "is_selected"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "has_capacity"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end
