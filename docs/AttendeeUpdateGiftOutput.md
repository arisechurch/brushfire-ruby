# Brushfire::AttendeeUpdateGiftOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**attendee_number** | **Integer** |  | [optional] 
**event_id** | **String** |  | [optional] 
**attendee_type_id** | **String** |  | [optional] 
**attendee_code** | **String** |  | [optional] 
**group_id** | **String** |  | [optional] 
**is_completed** | **BOOLEAN** |  | [optional] 
**order_id** | **String** |  | [optional] 
**current_order_id** | **String** |  | [optional] 
**original_order_id** | **String** |  | [optional] 
**cart_id** | **String** |  | [optional] 
**is_gift** | **BOOLEAN** |  | [optional] 
**is_pre_registered** | **BOOLEAN** |  | [optional] 
**recipient_email** | **String** |  | [optional] 


