# Brushfire::OrderSingleOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_key** | **String** |  | [optional] 
**object_number** | **Integer** |  | [optional] 
**attendee_count** | **Integer** |  | [optional] 
**ordered_at** | **DateTime** |  | [optional] 
**ordered_at_local** | **DateTime** |  | [optional] 
**billing_name** | **String** |  | [optional] 
**billing_address** | **String** |  | [optional] 
**billing_first_name** | **String** |  | [optional] 
**billing_last_name** | **String** |  | [optional] 
**billing_organization** | **String** |  | [optional] 
**billing_street1** | **String** |  | [optional] 
**billing_street2** | **String** |  | [optional] 
**billing_city** | **String** |  | [optional] 
**billing_region** | **String** |  | [optional] 
**billing_postal** | **String** |  | [optional] 
**billing_country** | **String** |  | [optional] 
**contact_phone** | **String** |  | [optional] 
**contact_email** | **String** |  | [optional] 
**payment_method** | **String** |  | [optional] 
**delivery_method** | **String** |  | [optional] 
**fields** | [**Array&lt;FieldOutput&gt;**](FieldOutput.md) |  | [optional] 
**attendees** | [**Array&lt;OrderAttendeeSummaryOutput&gt;**](OrderAttendeeSummaryOutput.md) |  | [optional] 
**qr_code** | **String** |  | [optional] 


