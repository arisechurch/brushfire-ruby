# Brushfire::AccessCodesApi

All URIs are relative to *https://api.brushfire.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**access_codes_create_dynamic**](AccessCodesApi.md#access_codes_create_dynamic) | **POST** /accesscodes/dynamic | Creates the specified quantity of dynamic access codes based upon the provided template code
[**access_codes_list**](AccessCodesApi.md#access_codes_list) | **GET** /accesscodes | List all access codes for a specific client (and additional parameters)
[**access_codes_single**](AccessCodesApi.md#access_codes_single) | **GET** /accesscodes/{accessCodeId} | Get details of a single access code


# **access_codes_create_dynamic**
> Array&lt;AccessCodeCreateOutput&gt; access_codes_create_dynamic(body)

Creates the specified quantity of dynamic access codes based upon the provided template code



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::AccessCodesApi.new

body = Brushfire::AccessCodeCreateInput.new # AccessCodeCreateInput | 


begin
  #Creates the specified quantity of dynamic access codes based upon the provided template code
  result = api_instance.access_codes_create_dynamic(body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling AccessCodesApi->access_codes_create_dynamic: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AccessCodeCreateInput**](AccessCodeCreateInput.md)|  | 

### Return type

[**Array&lt;AccessCodeCreateOutput&gt;**](AccessCodeCreateOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **access_codes_list**
> Array&lt;AccessCodeOutput&gt; access_codes_list(client_id, opts)

List all access codes for a specific client (and additional parameters)



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::AccessCodesApi.new

client_id = 'client_id_example' # String | A string, a GUID, or an integer that corresponds to a specific client

opts = { 
  kind: 'kind_example', # String | An option string value from \"Standard\", \"Template\", or \"Dynamic\" that represents the kind of access code
  is_redeemed: true, # BOOLEAN | An optional boolean value if the access code has been redeemed
  search: 'search_example' # String | An option string to filter access codes by name
}

begin
  #List all access codes for a specific client (and additional parameters)
  result = api_instance.access_codes_list(client_id, opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling AccessCodesApi->access_codes_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **String**| A string, a GUID, or an integer that corresponds to a specific client | 
 **kind** | **String**| An option string value from \&quot;Standard\&quot;, \&quot;Template\&quot;, or \&quot;Dynamic\&quot; that represents the kind of access code | [optional] 
 **is_redeemed** | **BOOLEAN**| An optional boolean value if the access code has been redeemed | [optional] 
 **search** | **String**| An option string to filter access codes by name | [optional] 

### Return type

[**Array&lt;AccessCodeOutput&gt;**](AccessCodeOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **access_codes_single**
> AccessCodeOutput access_codes_single(access_code_id)

Get details of a single access code



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::AccessCodesApi.new

access_code_id = 'access_code_id_example' # String | A GUID that corresponds to a specific access code


begin
  #Get details of a single access code
  result = api_instance.access_codes_single(access_code_id)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling AccessCodesApi->access_codes_single: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_code_id** | **String**| A GUID that corresponds to a specific access code | 

### Return type

[**AccessCodeOutput**](AccessCodeOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



