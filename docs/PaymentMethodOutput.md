# Brushfire::PaymentMethodOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_method_id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**image_url** | **String** |  | [optional] 
**is_electronic** | **BOOLEAN** |  | [optional] 
**info_grouping** | **String** |  | [optional] 
**grouping** | **String** |  | [optional] 


