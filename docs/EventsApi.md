# Brushfire::EventsApi

All URIs are relative to *https://api.brushfire.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**events_all_fields**](EventsApi.md#events_all_fields) | **GET** /events/{eventId}/fieldmetadata | Get the meta data for ALL fields for an event including the built-in/system ones
[**events_communities**](EventsApi.md#events_communities) | **GET** /events/{eventId}/communities | Returns all communities for the specified event
[**events_data**](EventsApi.md#events_data) | **GET** /events/{eventId}/data | Returns all data for the specified type
[**events_details**](EventsApi.md#events_details) | **GET** /events/{eventId}/details | Retrieve information for an event
[**events_documents**](EventsApi.md#events_documents) | **GET** /events/{eventId}/documents | Get which printing layouts are available for an event
[**events_fields**](EventsApi.md#events_fields) | **GET** /events/{eventId}/fields | Get fields for an event
[**events_get_sessions_by_event**](EventsApi.md#events_get_sessions_by_event) | **GET** /events/{eventId}/sessions | Get a list of all of the sessions for an event
[**events_groups**](EventsApi.md#events_groups) | **GET** /events/{eventId}/groups | List all groups for an event
[**events_list**](EventsApi.md#events_list) | **GET** /events | Get a list of events for the specified user.
[**events_lookup**](EventsApi.md#events_lookup) | **GET** /events/{eventId}/lookup | Look up attendees based upon email for an event
[**events_payment_methods**](EventsApi.md#events_payment_methods) | **GET** /events/{eventId}/paymentmethods | Get which payment methods are available for an event
[**events_report**](EventsApi.md#events_report) | **GET** /events/{eventId}/reports/{reportId} | Get report data for a custom or preset event report
[**events_resources**](EventsApi.md#events_resources) | **GET** /events/{eventId}/resources | Get a list of resource urls corresponding to specific Brushfire pages for the event
[**events_seats**](EventsApi.md#events_seats) | **GET** /events/{eventId}/seats | Get all the seats in a section for a specific event (and optionally finds best seats, per section or overall, but does not reserve them)
[**events_section_single**](EventsApi.md#events_section_single) | **GET** /events/{eventId}/sections/{sectionId} | Get the details for a specific section
[**events_sections_by_event**](EventsApi.md#events_sections_by_event) | **GET** /events/{eventId}/sections | Get the details for all the sections of an event
[**events_single**](EventsApi.md#events_single) | **GET** /events/{eventId} | Retrieve information for an event
[**events_types**](EventsApi.md#events_types) | **GET** /events/{eventId}/types | List all currently available attendee types for a specific event and account (and optionally a specific seat)


# **events_all_fields**
> Array&lt;FieldMetaOutput&gt; events_all_fields(event_id)

Get the meta data for ALL fields for an event including the built-in/system ones



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::EventsApi.new

event_id = 'event_id_example' # String | An integer or a GUID that corresponds to a specific event


begin
  #Get the meta data for ALL fields for an event including the built-in/system ones
  result = api_instance.events_all_fields(event_id)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling EventsApi->events_all_fields: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_id** | **String**| An integer or a GUID that corresponds to a specific event | 

### Return type

[**Array&lt;FieldMetaOutput&gt;**](FieldMetaOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **events_communities**
> Array&lt;EventCommunityOutput&gt; events_communities(event_id)

Returns all communities for the specified event



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::EventsApi.new

event_id = 'event_id_example' # String | An integer or a GUID that corresponds to a specific event


begin
  #Returns all communities for the specified event
  result = api_instance.events_communities(event_id)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling EventsApi->events_communities: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_id** | **String**| An integer or a GUID that corresponds to a specific event | 

### Return type

[**Array&lt;EventCommunityOutput&gt;**](EventCommunityOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **events_data**
> Object events_data(event_id, opts)

Returns all data for the specified type



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::EventsApi.new

event_id = 'event_id_example' # String | An integer or a GUID that corresponds to a specific event

opts = { 
  type: 'type_example', # String | A string value of \"Attendee\", \"Order\", or \"Group\"
  since: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | An optional date (UTC) from which to filter data
  include_cancelled: true # BOOLEAN | An optional boolean (default false) to include cancelled attendees
}

begin
  #Returns all data for the specified type
  result = api_instance.events_data(event_id, opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling EventsApi->events_data: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_id** | **String**| An integer or a GUID that corresponds to a specific event | 
 **type** | **String**| A string value of \&quot;Attendee\&quot;, \&quot;Order\&quot;, or \&quot;Group\&quot; | [optional] 
 **since** | **DateTime**| An optional date (UTC) from which to filter data | [optional] 
 **include_cancelled** | **BOOLEAN**| An optional boolean (default false) to include cancelled attendees | [optional] 

### Return type

**Object**

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **events_details**
> EventDetailsOutput events_details(event_id, opts)

Retrieve information for an event



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::EventsApi.new

event_id = 'event_id_example' # String | An integer or a GUID that corresponds to a specific event

opts = { 
  group_id: 'group_id_example', # String | An integer or a GUID that corresponds to a specific group
  access_key: 'access_key_example', # String | The Access Key for the user context
  access: 'access_example' # String | The Access Code for accessing the event
}

begin
  #Retrieve information for an event
  result = api_instance.events_details(event_id, opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling EventsApi->events_details: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_id** | **String**| An integer or a GUID that corresponds to a specific event | 
 **group_id** | **String**| An integer or a GUID that corresponds to a specific group | [optional] 
 **access_key** | **String**| The Access Key for the user context | [optional] 
 **access** | **String**| The Access Code for accessing the event | [optional] 

### Return type

[**EventDetailsOutput**](EventDetailsOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **events_documents**
> EventDocumentOutput events_documents(event_id)

Get which printing layouts are available for an event



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::EventsApi.new

event_id = 'event_id_example' # String | An integer or a GUID that corresponds to a specific event


begin
  #Get which printing layouts are available for an event
  result = api_instance.events_documents(event_id)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling EventsApi->events_documents: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_id** | **String**| An integer or a GUID that corresponds to a specific event | 

### Return type

[**EventDocumentOutput**](EventDocumentOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **events_fields**
> Array&lt;FieldOutput&gt; events_fields(event_id, field_type, opts)

Get fields for an event



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::EventsApi.new

event_id = 'event_id_example' # String | An integer or a GUID that corresponds to a specific event

field_type = 'field_type_example' # String | One of \"Attendees\", \"Groups\", or \"Buyers\" for the type of fields you want

opts = { 
  access_key: 'access_key_example' # String | The Access Key for the user context
}

begin
  #Get fields for an event
  result = api_instance.events_fields(event_id, field_type, opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling EventsApi->events_fields: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_id** | **String**| An integer or a GUID that corresponds to a specific event | 
 **field_type** | **String**| One of \&quot;Attendees\&quot;, \&quot;Groups\&quot;, or \&quot;Buyers\&quot; for the type of fields you want | 
 **access_key** | **String**| The Access Key for the user context | [optional] 

### Return type

[**Array&lt;FieldOutput&gt;**](FieldOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **events_get_sessions_by_event**
> Array&lt;EventSessionOutput&gt; events_get_sessions_by_event(event_id, opts)

Get a list of all of the sessions for an event



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::EventsApi.new

event_id = 'event_id_example' # String | An integer or a GUID that corresponds to a specific event

opts = { 
  search: 'search_example' # String | A string to filter sessions
}

begin
  #Get a list of all of the sessions for an event
  result = api_instance.events_get_sessions_by_event(event_id, opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling EventsApi->events_get_sessions_by_event: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_id** | **String**| An integer or a GUID that corresponds to a specific event | 
 **search** | **String**| A string to filter sessions | [optional] 

### Return type

[**Array&lt;EventSessionOutput&gt;**](EventSessionOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **events_groups**
> Array&lt;GroupSummaryOutput&gt; events_groups(event_id, opts)

List all groups for an event



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::EventsApi.new

event_id = 'event_id_example' # String | An integer or a GUID that corresponds to a specific event

opts = { 
  search: 'search_example' # String | A string to filter groups
}

begin
  #List all groups for an event
  result = api_instance.events_groups(event_id, opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling EventsApi->events_groups: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_id** | **String**| An integer or a GUID that corresponds to a specific event | 
 **search** | **String**| A string to filter groups | [optional] 

### Return type

[**Array&lt;GroupSummaryOutput&gt;**](GroupSummaryOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **events_list**
> Array&lt;EventListOutput&gt; events_list(opts)

Get a list of events for the specified user.



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::EventsApi.new

opts = { 
  access_key: 'access_key_example', # String | The Access Key for the user context
  search: 'search_example', # String | A string to filter events
  inactive: true, # BOOLEAN | Boolean pass in true to return inactive events.
  qty: 56, # Integer | An integer to limit the number of results returned
  skip: 56 # Integer | An integer to skip the first events returned
}

begin
  #Get a list of events for the specified user.
  result = api_instance.events_list(opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling EventsApi->events_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **access_key** | **String**| The Access Key for the user context | [optional] 
 **search** | **String**| A string to filter events | [optional] 
 **inactive** | **BOOLEAN**| Boolean pass in true to return inactive events. | [optional] 
 **qty** | **Integer**| An integer to limit the number of results returned | [optional] 
 **skip** | **Integer**| An integer to skip the first events returned | [optional] 

### Return type

[**Array&lt;EventListOutput&gt;**](EventListOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **events_lookup**
> Array&lt;EventLookupOutput&gt; events_lookup(event_id, email)

Look up attendees based upon email for an event



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::EventsApi.new

event_id = 'event_id_example' # String | An integer or a GUID that corresponds to a specific event

email = 'email_example' # String | The email of the attendee, buyer, or group manager


begin
  #Look up attendees based upon email for an event
  result = api_instance.events_lookup(event_id, email)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling EventsApi->events_lookup: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_id** | **String**| An integer or a GUID that corresponds to a specific event | 
 **email** | **String**| The email of the attendee, buyer, or group manager | 

### Return type

[**Array&lt;EventLookupOutput&gt;**](EventLookupOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **events_payment_methods**
> Array&lt;EventPaymentMethodOutput&gt; events_payment_methods(event_id, opts)

Get which payment methods are available for an event



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::EventsApi.new

event_id = 'event_id_example' # String | An integer or a GUID that corresponds to a specific event

opts = { 
  access_key: 'access_key_example' # String | The Access Key for the user context
}

begin
  #Get which payment methods are available for an event
  result = api_instance.events_payment_methods(event_id, opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling EventsApi->events_payment_methods: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_id** | **String**| An integer or a GUID that corresponds to a specific event | 
 **access_key** | **String**| The Access Key for the user context | [optional] 

### Return type

[**Array&lt;EventPaymentMethodOutput&gt;**](EventPaymentMethodOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **events_report**
> Object events_report(event_id, report_id)

Get report data for a custom or preset event report



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::EventsApi.new

event_id = 'event_id_example' # String | An integer or a GUID that corresponds to a specific event

report_id = 'report_id_example' # String | A GUID that corresponds to a specific report or a string that corresponds to a specific report preset


begin
  #Get report data for a custom or preset event report
  result = api_instance.events_report(event_id, report_id)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling EventsApi->events_report: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_id** | **String**| An integer or a GUID that corresponds to a specific event | 
 **report_id** | **String**| A GUID that corresponds to a specific report or a string that corresponds to a specific report preset | 

### Return type

**Object**

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **events_resources**
> Array&lt;EventResourceOutput&gt; events_resources(event_id)

Get a list of resource urls corresponding to specific Brushfire pages for the event



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::EventsApi.new

event_id = 'event_id_example' # String | An integer or a GUID that corresponds to a specific event


begin
  #Get a list of resource urls corresponding to specific Brushfire pages for the event
  result = api_instance.events_resources(event_id)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling EventsApi->events_resources: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_id** | **String**| An integer or a GUID that corresponds to a specific event | 

### Return type

[**Array&lt;EventResourceOutput&gt;**](EventResourceOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **events_seats**
> Array&lt;EventSeatOutput&gt; events_seats(event_id, opts)

Get all the seats in a section for a specific event (and optionally finds best seats, per section or overall, but does not reserve them)



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::EventsApi.new

event_id = 'event_id_example' # String | An integer or a GUID that corresponds to a specific event

opts = { 
  section_id: 'section_id_example', # String | A string (the exact name) or a GUID representing the section
  quantity: 56, # Integer | Optional integer to find a quantity of seats
  access_key: 'access_key_example', # String | The Access Key for the user context
  type_id: 'type_id_example', # String | Optional GUID to find seats in a particular type
  group_id: 'group_id_example', # String | An integer or a GUID that corresponds to a specific group
  seat_code: 'seat_code_example', # String | A seat-level access code to unlock seats
  nonconsecutive: true, # BOOLEAN | Optional boolean to find seats that are non-consecutive
  base_price: 1.2 # Float | Optional decimal to find seats in a particular base price
}

begin
  #Get all the seats in a section for a specific event (and optionally finds best seats, per section or overall, but does not reserve them)
  result = api_instance.events_seats(event_id, opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling EventsApi->events_seats: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_id** | **String**| An integer or a GUID that corresponds to a specific event | 
 **section_id** | **String**| A string (the exact name) or a GUID representing the section | [optional] 
 **quantity** | **Integer**| Optional integer to find a quantity of seats | [optional] 
 **access_key** | **String**| The Access Key for the user context | [optional] 
 **type_id** | **String**| Optional GUID to find seats in a particular type | [optional] 
 **group_id** | **String**| An integer or a GUID that corresponds to a specific group | [optional] 
 **seat_code** | **String**| A seat-level access code to unlock seats | [optional] 
 **nonconsecutive** | **BOOLEAN**| Optional boolean to find seats that are non-consecutive | [optional] 
 **base_price** | **Float**| Optional decimal to find seats in a particular base price | [optional] 

### Return type

[**Array&lt;EventSeatOutput&gt;**](EventSeatOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **events_section_single**
> EventSectionOutput events_section_single(event_id, section_id)

Get the details for a specific section



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::EventsApi.new

event_id = 'event_id_example' # String | An integer or a GUID that corresponds to a specific event

section_id = 'section_id_example' # String | A string (the exact name) or a GUID representing the section


begin
  #Get the details for a specific section
  result = api_instance.events_section_single(event_id, section_id)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling EventsApi->events_section_single: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_id** | **String**| An integer or a GUID that corresponds to a specific event | 
 **section_id** | **String**| A string (the exact name) or a GUID representing the section | 

### Return type

[**EventSectionOutput**](EventSectionOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **events_sections_by_event**
> Array&lt;EventSectionOutput&gt; events_sections_by_event(event_id)

Get the details for all the sections of an event



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::EventsApi.new

event_id = 'event_id_example' # String | An integer or a GUID that corresponds to a specific event


begin
  #Get the details for all the sections of an event
  result = api_instance.events_sections_by_event(event_id)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling EventsApi->events_sections_by_event: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_id** | **String**| An integer or a GUID that corresponds to a specific event | 

### Return type

[**Array&lt;EventSectionOutput&gt;**](EventSectionOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **events_single**
> EventSingleOutput events_single(event_id)

Retrieve information for an event



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::EventsApi.new

event_id = 'event_id_example' # String | An integer or a GUID that corresponds to a specific event


begin
  #Retrieve information for an event
  result = api_instance.events_single(event_id)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling EventsApi->events_single: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_id** | **String**| An integer or a GUID that corresponds to a specific event | 

### Return type

[**EventSingleOutput**](EventSingleOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **events_types**
> Array&lt;EventTypeOutput&gt; events_types(event_id, opts)

List all currently available attendee types for a specific event and account (and optionally a specific seat)



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::EventsApi.new

event_id = 'event_id_example' # String | An integer or a GUID that corresponds to a specific event

opts = { 
  group: 'group_example', # String | An integer or a GUID that corresponds to a specific group
  access_key: 'access_key_example', # String | Access Key for user account
  access: 'access_example', # String | The Access Code for accessing the types
  seat: 'seat_example' # String | A GUID that corresponds to a specific seat, if blank, will return all types for the event
}

begin
  #List all currently available attendee types for a specific event and account (and optionally a specific seat)
  result = api_instance.events_types(event_id, opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling EventsApi->events_types: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_id** | **String**| An integer or a GUID that corresponds to a specific event | 
 **group** | **String**| An integer or a GUID that corresponds to a specific group | [optional] 
 **access_key** | **String**| Access Key for user account | [optional] 
 **access** | **String**| The Access Code for accessing the types | [optional] 
 **seat** | **String**| A GUID that corresponds to a specific seat, if blank, will return all types for the event | [optional] 

### Return type

[**Array&lt;EventTypeOutput&gt;**](EventTypeOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



