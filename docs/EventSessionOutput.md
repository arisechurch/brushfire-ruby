# Brushfire::EventSessionOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**session_number** | **Integer** |  | [optional] 
**name** | **String** |  | [optional] 
**attendee_total** | **Integer** |  | [optional] 
**checked_in_count** | **Integer** |  | [optional] 
**type** | **String** |  | [optional] 


