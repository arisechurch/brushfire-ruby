# Brushfire::AccountSingleOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**account_number** | **Integer** |  | [optional] 
**email** | **String** |  | [optional] 
**first_name** | **String** |  | [optional] 
**last_name** | **String** |  | [optional] 
**street1** | **String** |  | [optional] 
**street2** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**region** | **String** |  | [optional] 
**country** | **String** |  | [optional] 
**postal_code** | **String** |  | [optional] 
**phone** | **String** |  | [optional] 
**address** | **String** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**is_user** | **BOOLEAN** |  | [optional] 
**avatar_url** | **String** |  | [optional] 


