# Brushfire::PaymentProfileSingleOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**account_name** | **String** |  | [optional] 
**gateway_id** | **String** |  | [optional] 
**gateway_name** | **String** |  | [optional] 
**supports_auto_fees** | **BOOLEAN** |  | [optional] 
**supports_sideloading** | **BOOLEAN** |  | [optional] 
**allows_reference_sales** | **BOOLEAN** |  | [optional] 
**allows_standalone_credits** | **BOOLEAN** |  | [optional] 
**reference_day_limit** | **Integer** |  | [optional] 
**options** | **Hash&lt;String, String&gt;** |  | [optional] 
**electronic_payment_methods** | [**Array&lt;PaymentMethodListOutput&gt;**](PaymentMethodListOutput.md) |  | [optional] 


