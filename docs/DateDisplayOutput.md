# Brushfire::DateDisplayOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**months** | **String** |  | [optional] 
**dates** | **String** |  | [optional] 
**days** | **String** |  | [optional] 
**times** | **String** |  | [optional] 
**is_ongoing** | **BOOLEAN** |  | [optional] 


