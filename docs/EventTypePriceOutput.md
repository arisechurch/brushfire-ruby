# Brushfire::EventTypePriceOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price_id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**base_price** | **Float** |  | [optional] 
**amount** | **Float** |  | [optional] 
**fee** | **Float** |  | [optional] 


