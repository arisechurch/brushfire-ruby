# Brushfire::SessionCheckoutMultipleOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | **BOOLEAN** |  | [optional] 
**checked_in_at** | **DateTime** |  | [optional] 
**message** | **String** |  | [optional] 
**code** | **String** |  | [optional] 


