# Brushfire::SessionsApi

All URIs are relative to *https://api.brushfire.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sessions_checkin_multiple**](SessionsApi.md#sessions_checkin_multiple) | **POST** /sessions/{sessionId}/checkin | Check in one or more attendees for a specific session
[**sessions_checkout_multiple**](SessionsApi.md#sessions_checkout_multiple) | **POST** /sessions/{sessionId}/checkout | Check out one or more attendees for a specific session
[**sessions_email**](SessionsApi.md#sessions_email) | **GET** /sessions/{sessionId}/email/{email} | Retrieve attendee data from a specific email and session
[**sessions_get_attendees_for_session**](SessionsApi.md#sessions_get_attendees_for_session) | **GET** /sessions/{sessionId}/attendees | Get a list of all of the attendees for a given check-in session
[**sessions_get_session_by_number**](SessionsApi.md#sessions_get_session_by_number) | **GET** /sessions/{sessionId} | Get detailed information about a given session
[**sessions_group_details**](SessionsApi.md#sessions_group_details) | **GET** /sessions/{sessionId}/groups/{groupId} | Get all details, including checkin info for a group in a given session
[**sessions_order_details**](SessionsApi.md#sessions_order_details) | **GET** /sessions/{sessionId}/orders/{orderId} | Retrieve Order and Buyer data from a specific order and session


# **sessions_checkin_multiple**
> Array&lt;SessionCheckinMultipleOutput&gt; sessions_checkin_multiple(session_id, body)

Check in one or more attendees for a specific session



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::SessionsApi.new

session_id = 'session_id_example' # String | An integer representing the session to check into

body = Brushfire::SessionCheckinMultipleInput.new # SessionCheckinMultipleInput | 


begin
  #Check in one or more attendees for a specific session
  result = api_instance.sessions_checkin_multiple(session_id, body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling SessionsApi->sessions_checkin_multiple: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id** | **String**| An integer representing the session to check into | 
 **body** | [**SessionCheckinMultipleInput**](SessionCheckinMultipleInput.md)|  | 

### Return type

[**Array&lt;SessionCheckinMultipleOutput&gt;**](SessionCheckinMultipleOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **sessions_checkout_multiple**
> Array&lt;SessionCheckoutMultipleOutput&gt; sessions_checkout_multiple(session_id, body)

Check out one or more attendees for a specific session



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::SessionsApi.new

session_id = 'session_id_example' # String | An integer representing the session to check out of

body = Brushfire::SessionCheckoutMultipleInput.new # SessionCheckoutMultipleInput | 


begin
  #Check out one or more attendees for a specific session
  result = api_instance.sessions_checkout_multiple(session_id, body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling SessionsApi->sessions_checkout_multiple: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id** | **String**| An integer representing the session to check out of | 
 **body** | [**SessionCheckoutMultipleInput**](SessionCheckoutMultipleInput.md)|  | 

### Return type

[**Array&lt;SessionCheckoutMultipleOutput&gt;**](SessionCheckoutMultipleOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **sessions_email**
> Array&lt;AttendeeSessionOutput&gt; sessions_email(email, session_id)

Retrieve attendee data from a specific email and session



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::SessionsApi.new

email = 'email_example' # String | A string representing an email address

session_id = 789 # Integer | An integer representing a specific session


begin
  #Retrieve attendee data from a specific email and session
  result = api_instance.sessions_email(email, session_id)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling SessionsApi->sessions_email: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| A string representing an email address | 
 **session_id** | **Integer**| An integer representing a specific session | 

### Return type

[**Array&lt;AttendeeSessionOutput&gt;**](AttendeeSessionOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **sessions_get_attendees_for_session**
> Array&lt;AttendeeSessionOutput&gt; sessions_get_attendees_for_session(session_id, opts)

Get a list of all of the attendees for a given check-in session



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::SessionsApi.new

session_id = 'session_id_example' # String | The session number for which to retrieve attendees

opts = { 
  search: 'search_example', # String | An optional search string for filtering attendees
  qty: 56, # Integer | An integer to limit the number of results returned
  skip: 56 # Integer | An integer to skip the first attendees returned
}

begin
  #Get a list of all of the attendees for a given check-in session
  result = api_instance.sessions_get_attendees_for_session(session_id, opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling SessionsApi->sessions_get_attendees_for_session: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id** | **String**| The session number for which to retrieve attendees | 
 **search** | **String**| An optional search string for filtering attendees | [optional] 
 **qty** | **Integer**| An integer to limit the number of results returned | [optional] 
 **skip** | **Integer**| An integer to skip the first attendees returned | [optional] 

### Return type

[**Array&lt;AttendeeSessionOutput&gt;**](AttendeeSessionOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **sessions_get_session_by_number**
> EventSessionOutput sessions_get_session_by_number(session_id)

Get detailed information about a given session



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::SessionsApi.new

session_id = 'session_id_example' # String | An integer that corresponds to a specific session


begin
  #Get detailed information about a given session
  result = api_instance.sessions_get_session_by_number(session_id)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling SessionsApi->sessions_get_session_by_number: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_id** | **String**| An integer that corresponds to a specific session | 

### Return type

[**EventSessionOutput**](EventSessionOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **sessions_group_details**
> SessionGroupDetailOutput sessions_group_details(group_id, session_id)

Get all details, including checkin info for a group in a given session



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::SessionsApi.new

group_id = 'group_id_example' # String | An integer or GUID representing a specific group

session_id = 789 # Integer | An integer representing a specific session


begin
  #Get all details, including checkin info for a group in a given session
  result = api_instance.sessions_group_details(group_id, session_id)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling SessionsApi->sessions_group_details: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **String**| An integer or GUID representing a specific group | 
 **session_id** | **Integer**| An integer representing a specific session | 

### Return type

[**SessionGroupDetailOutput**](SessionGroupDetailOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **sessions_order_details**
> SessionOrderDetailOutput sessions_order_details(order_id, session_id)

Retrieve Order and Buyer data from a specific order and session



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::SessionsApi.new

order_id = 'order_id_example' # String | A string (orderKey) or a GUID representing a specific order

session_id = 789 # Integer | An integer representing a specific session


begin
  #Retrieve Order and Buyer data from a specific order and session
  result = api_instance.sessions_order_details(order_id, session_id)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling SessionsApi->sessions_order_details: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_id** | **String**| A string (orderKey) or a GUID representing a specific order | 
 **session_id** | **Integer**| An integer representing a specific session | 

### Return type

[**SessionOrderDetailOutput**](SessionOrderDetailOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



