# Brushfire::CartAddAttendeeItemInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_id** | **String** |  | 
**type_id** | **String** |  | 
**quantity** | **Integer** |  | 
**group_id** | **String** |  | [optional] 
**amount** | **Float** |  | [optional] 
**is_pre_registered** | **BOOLEAN** |  | [optional] 
**custom_amount** | **Float** |  | [optional] 
**selected_amount** | **Float** |  | [optional] 
**seat_id** | **String** |  | [optional] 


