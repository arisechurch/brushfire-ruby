# Brushfire::EventSectionPriceOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price_id** | **String** |  | [optional] 
**base_price** | **Float** |  | [optional] 
**available_spots** | **Integer** |  | [optional] 


