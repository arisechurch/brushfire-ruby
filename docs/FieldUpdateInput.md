# Brushfire::FieldUpdateInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_key** | **String** |  | [optional] 
**field_values** | [**Array&lt;FieldInput&gt;**](FieldInput.md) |  | [optional] 
**update_new_only** | **BOOLEAN** |  | [optional] 


