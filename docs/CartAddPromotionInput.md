# Brushfire::CartAddPromotionInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code_name** | **String** |  | 
**access_key** | **String** |  | [optional] 


