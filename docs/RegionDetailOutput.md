# Brushfire::RegionDetailOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**iso** | **Integer** |  | [optional] 
**regions** | [**Array&lt;RegionDetailOutput&gt;**](RegionDetailOutput.md) |  | [optional] 


