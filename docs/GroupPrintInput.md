# Brushfire::GroupPrintInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | 
**groups** | **Array&lt;String&gt;** |  | 
**event_id** | **String** |  | 


