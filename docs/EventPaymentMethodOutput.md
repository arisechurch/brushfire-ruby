# Brushfire::EventPaymentMethodOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**grouping** | **String** |  | [optional] 
**id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**image_url** | **String** |  | [optional] 


