# Brushfire::CartDeleteAttendeeOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expires_at** | **DateTime** |  | [optional] 
**total_count** | **Integer** |  | [optional] 
**deleted_count** | **Integer** |  | [optional] 


