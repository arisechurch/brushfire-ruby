# Brushfire::OrdersApi

All URIs are relative to *https://api.brushfire.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orders_create**](OrdersApi.md#orders_create) | **POST** /orders | Create an order from an existing cart
[**orders_create_direct**](OrdersApi.md#orders_create_direct) | **POST** /orders/direct | Create cart-less order for free, general admission events with no fields.
[**orders_create_pos**](OrdersApi.md#orders_create_pos) | **POST** /orders/pos | Create a point of sale order from an existing cart
[**orders_fields**](OrdersApi.md#orders_fields) | **GET** /orders/{orderId}/fields | Retrieve fields for an order
[**orders_fields_update**](OrdersApi.md#orders_fields_update) | **POST** /orders/{orderId}/fields | Update fields for an order
[**orders_helpdesk**](OrdersApi.md#orders_helpdesk) | **GET** /orders/helpdesk/{email} | Retrieve orders that match provided email
[**orders_print**](OrdersApi.md#orders_print) | **GET** /orders/{orderId}/print | Retrieve the printable resources for a single order
[**orders_print_all**](OrdersApi.md#orders_print_all) | **POST** /orders/print | Retrieve the printable resources for multiple orders
[**orders_single**](OrdersApi.md#orders_single) | **GET** /orders/{orderId} | Retrieve a single order


# **orders_create**
> Array&lt;OrderCreateOutput&gt; orders_create(body)

Create an order from an existing cart



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::OrdersApi.new

body = Brushfire::OrderCreateInput.new # OrderCreateInput | 


begin
  #Create an order from an existing cart
  result = api_instance.orders_create(body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling OrdersApi->orders_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**OrderCreateInput**](OrderCreateInput.md)|  | 

### Return type

[**Array&lt;OrderCreateOutput&gt;**](OrderCreateOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **orders_create_direct**
> OrderCreateOutput orders_create_direct(body)

Create cart-less order for free, general admission events with no fields.



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::OrdersApi.new

body = Brushfire::OrderCreateDirectInput.new # OrderCreateDirectInput | 


begin
  #Create cart-less order for free, general admission events with no fields.
  result = api_instance.orders_create_direct(body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling OrdersApi->orders_create_direct: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**OrderCreateDirectInput**](OrderCreateDirectInput.md)|  | 

### Return type

[**OrderCreateOutput**](OrderCreateOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **orders_create_pos**
> Array&lt;OrderCreateOutput&gt; orders_create_pos(body)

Create a point of sale order from an existing cart



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::OrdersApi.new

body = Brushfire::OrderCreatePOSInput.new # OrderCreatePOSInput | 


begin
  #Create a point of sale order from an existing cart
  result = api_instance.orders_create_pos(body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling OrdersApi->orders_create_pos: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**OrderCreatePOSInput**](OrderCreatePOSInput.md)|  | 

### Return type

[**Array&lt;OrderCreateOutput&gt;**](OrderCreateOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **orders_fields**
> Array&lt;FieldOutput&gt; orders_fields(order_id, opts)

Retrieve fields for an order



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::OrdersApi.new

order_id = 'order_id_example' # String | A string (orderKey) or a GUID representing a specific order

opts = { 
  access_key: 'access_key_example' # String | The Access Key for the user context
}

begin
  #Retrieve fields for an order
  result = api_instance.orders_fields(order_id, opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling OrdersApi->orders_fields: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_id** | **String**| A string (orderKey) or a GUID representing a specific order | 
 **access_key** | **String**| The Access Key for the user context | [optional] 

### Return type

[**Array&lt;FieldOutput&gt;**](FieldOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **orders_fields_update**
> Array&lt;FieldOutput&gt; orders_fields_update(order_id, body)

Update fields for an order



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::OrdersApi.new

order_id = 'order_id_example' # String | A string (orderKey) or a GUID representing a specific order

body = Brushfire::FieldUpdateInput.new # FieldUpdateInput | 


begin
  #Update fields for an order
  result = api_instance.orders_fields_update(order_id, body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling OrdersApi->orders_fields_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_id** | **String**| A string (orderKey) or a GUID representing a specific order | 
 **body** | [**FieldUpdateInput**](FieldUpdateInput.md)|  | 

### Return type

[**Array&lt;FieldOutput&gt;**](FieldOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **orders_helpdesk**
> Array&lt;OrderHelpdeskOutput&gt; orders_helpdesk(email)

Retrieve orders that match provided email



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::OrdersApi.new

email = 'email_example' # String | An email to search across order email fields


begin
  #Retrieve orders that match provided email
  result = api_instance.orders_helpdesk(email)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling OrdersApi->orders_helpdesk: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| An email to search across order email fields | 

### Return type

[**Array&lt;OrderHelpdeskOutput&gt;**](OrderHelpdeskOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **orders_print**
> String orders_print(order_id, type)

Retrieve the printable resources for a single order



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::OrdersApi.new

order_id = 'order_id_example' # String | A string (orderKey) or a GUID representing a specific order

type = 'type_example' # String | A string that is one of \"Ticket\", \"ETicket\", or \"Namebadge\"


begin
  #Retrieve the printable resources for a single order
  result = api_instance.orders_print(order_id, type)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling OrdersApi->orders_print: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_id** | **String**| A string (orderKey) or a GUID representing a specific order | 
 **type** | **String**| A string that is one of \&quot;Ticket\&quot;, \&quot;ETicket\&quot;, or \&quot;Namebadge\&quot; | 

### Return type

**String**

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **orders_print_all**
> String orders_print_all(body)

Retrieve the printable resources for multiple orders



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::OrdersApi.new

body = Brushfire::OrderPrintInput.new # OrderPrintInput | 


begin
  #Retrieve the printable resources for multiple orders
  result = api_instance.orders_print_all(body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling OrdersApi->orders_print_all: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**OrderPrintInput**](OrderPrintInput.md)|  | 

### Return type

**String**

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **orders_single**
> OrderSingleOutput orders_single(order_id, opts)

Retrieve a single order



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::OrdersApi.new

order_id = 'order_id_example' # String | A string (orderKey) or a GUID representing a specific order

opts = { 
  access_key: 'access_key_example', # String | The Access Key forthe user context
  qr_size: 56 # Integer | The width and height of the QR Code. If blank, no QR will be sent.
}

begin
  #Retrieve a single order
  result = api_instance.orders_single(order_id, opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling OrdersApi->orders_single: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_id** | **String**| A string (orderKey) or a GUID representing a specific order | 
 **access_key** | **String**| The Access Key forthe user context | [optional] 
 **qr_size** | **Integer**| The width and height of the QR Code. If blank, no QR will be sent. | [optional] 

### Return type

[**OrderSingleOutput**](OrderSingleOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



