# Brushfire::CartDeletePromotionOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deleted_count** | **Integer** |  | [optional] 


