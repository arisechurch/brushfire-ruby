# Brushfire::PromotionCreateInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**template_id** | **String** |  | 
**quantity** | **Integer** |  | 
**prefix** | **String** |  | 


