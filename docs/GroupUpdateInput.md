# Brushfire::GroupUpdateInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**group_id** | **String** |  | 
**attendee_type_id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**manage_code** | **String** |  | [optional] 
**join_code** | **String** |  | [optional] 
**access_key** | **String** |  | [optional] 
**community_id** | **String** |  | [optional] 
**field_values** | [**Array&lt;FieldInput&gt;**](FieldInput.md) |  | [optional] 
**update_new_only** | **BOOLEAN** |  | [optional] 
**override_capacity** | **BOOLEAN** |  | [optional] 
**dont_follow_rules** | **BOOLEAN** |  | [optional] 


