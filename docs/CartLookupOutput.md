# Brushfire::CartLookupOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_number** | **Integer** |  | [optional] 
**is_legacy** | **BOOLEAN** |  | [optional] 
**first_name** | **String** |  | [optional] 
**last_name** | **String** |  | [optional] 
**organization** | **String** |  | [optional] 
**street1** | **String** |  | [optional] 
**street2** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**region** | **String** |  | [optional] 
**country** | **String** |  | [optional] 
**postal_code** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**phone** | **String** |  | [optional] 
**relevancy** | **Float** |  | [optional] 


