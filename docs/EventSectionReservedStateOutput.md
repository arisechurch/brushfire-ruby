# Brushfire::EventSectionReservedStateOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reserved_state** | [**SeatReservedStateOutput**](SeatReservedStateOutput.md) |  | [optional] 
**seat_count** | **Integer** |  | [optional] 


