# Brushfire::PaymentProfilesApi

All URIs are relative to *https://api.brushfire.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**payment_profiles_list**](PaymentProfilesApi.md#payment_profiles_list) | **GET** /paymentprofiles | Returns all payment profiles for the specified client
[**payment_profiles_single**](PaymentProfilesApi.md#payment_profiles_single) | **GET** /paymentprofiles/{paymentProfileId} | Returns the details of the specified payment profile


# **payment_profiles_list**
> Array&lt;PaymentProfileListOutput&gt; payment_profiles_list(client_id)

Returns all payment profiles for the specified client



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::PaymentProfilesApi.new

client_id = 'client_id_example' # String | A string, a GUID, or an integer that corresponds to a specific client


begin
  #Returns all payment profiles for the specified client
  result = api_instance.payment_profiles_list(client_id)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling PaymentProfilesApi->payment_profiles_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **String**| A string, a GUID, or an integer that corresponds to a specific client | 

### Return type

[**Array&lt;PaymentProfileListOutput&gt;**](PaymentProfileListOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **payment_profiles_single**
> PaymentProfileSingleOutput payment_profiles_single(payment_profile_id)

Returns the details of the specified payment profile



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::PaymentProfilesApi.new

payment_profile_id = 'payment_profile_id_example' # String | A GUID that corresponds to a specific payment profile


begin
  #Returns the details of the specified payment profile
  result = api_instance.payment_profiles_single(payment_profile_id)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling PaymentProfilesApi->payment_profiles_single: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **payment_profile_id** | **String**| A GUID that corresponds to a specific payment profile | 

### Return type

[**PaymentProfileSingleOutput**](PaymentProfileSingleOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



