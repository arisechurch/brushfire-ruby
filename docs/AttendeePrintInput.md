# Brushfire::AttendeePrintInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | 
**attendees** | **Array&lt;Integer&gt;** |  | 
**event_id** | **String** |  | 


