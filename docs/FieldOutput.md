# Brushfire::FieldOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**label** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**value** | **Object** |  | [optional] 
**display_value** | **String** |  | [optional] 
**validation_expression** | **String** |  | [optional] 
**is_required** | **BOOLEAN** |  | [optional] 
**notes** | **String** |  | [optional] 
**error_message** | **String** |  | [optional] 
**is_disabled** | **BOOLEAN** |  | [optional] 
**special_field_id** | **String** |  | [optional] 
**parent_option_id** | **String** |  | [optional] 
**parent_field_id** | **String** |  | [optional] 
**form_object_id** | **String** |  | [optional] 
**options** | [**Array&lt;FieldOptionFullOutput&gt;**](FieldOptionFullOutput.md) |  | [optional] 
**parent_option_is_selected** | **BOOLEAN** |  | [optional] 
**has_error** | **BOOLEAN** |  | [optional] 
**has_relevant_output** | **BOOLEAN** |  | [optional] 
**min_date** | **DateTime** |  | [optional] 
**max_date** | **DateTime** |  | [optional] 
**attendee_types** | **Array&lt;String&gt;** |  | [optional] 
**remote_url** | **String** |  | [optional] 
**selected_option_id** | **String** |  | [optional] 


