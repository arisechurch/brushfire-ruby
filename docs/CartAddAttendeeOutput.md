# Brushfire::CartAddAttendeeOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cart_id** | **String** |  | [optional] 
**expires_at** | **DateTime** |  | [optional] 
**total_count** | **Integer** |  | [optional] 
**added_count** | **Integer** |  | [optional] 
**event_cart** | [**CartEventOutput**](CartEventOutput.md) |  | [optional] 


