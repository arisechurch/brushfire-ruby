# Brushfire::ClientListOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**client_number** | **Integer** |  | [optional] 
**client_key** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**city** | **String** |  | [optional] 
**region** | **String** |  | [optional] 
**country** | **String** |  | [optional] 
**is_live** | **BOOLEAN** |  | [optional] 
**is_deleted** | **BOOLEAN** |  | [optional] 


