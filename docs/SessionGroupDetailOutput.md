# Brushfire::SessionGroupDetailOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**group_number** | **Integer** |  | [optional] 
**event_id** | **String** |  | [optional] 
**attendee_type_id** | **String** |  | [optional] 
**type_name** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**attendee_count** | **Integer** |  | [optional] 
**community_id** | **String** |  | [optional] 
**community_partition** | **String** |  | [optional] 
**community_name** | **String** |  | [optional] 
**fields** | [**Array&lt;FieldDisplayOutput&gt;**](FieldDisplayOutput.md) |  | [optional] 
**attendees** | [**Array&lt;AttendeeSessionOutput&gt;**](AttendeeSessionOutput.md) |  | [optional] 


