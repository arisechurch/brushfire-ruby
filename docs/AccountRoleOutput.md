# Brushfire::AccountRoleOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | **String** |  | [optional] 
**client_key** | **String** |  | [optional] 
**client_number** | **Integer** |  | [optional] 
**client_name** | **String** |  | [optional] 
**client_city** | **String** |  | [optional] 
**client_region** | **String** |  | [optional] 
**client_country** | **String** |  | [optional] 
**level** | **String** |  | [optional] 


