# Brushfire::HooksApi

All URIs are relative to *https://api.brushfire.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**hooks_ping**](HooksApi.md#hooks_ping) | **GET** /hooks/ping | For 3rd party and custom integrations to test connectivity after authentication
[**hooks_poll**](HooksApi.md#hooks_poll) | **GET** /hooks/poll | For 3rd party and custom integrations to view shape for &#x60;data&#x60; property of POST from Brushfire
[**hooks_sample**](HooksApi.md#hooks_sample) | **GET** /hooks/sample | For 3rd party and custom integrations to view shape for &#x60;data&#x60; property of POST from Brushfire
[**hooks_subscribe**](HooksApi.md#hooks_subscribe) | **POST** /hooks/subscribe | For 3rd party and custom integrations to subscribe to our notifications
[**hooks_unsubscribe**](HooksApi.md#hooks_unsubscribe) | **POST** /hooks/unsubscribe | For 3rd party and custom integrations to unsubscribe from our notifications


# **hooks_ping**
> Array&lt;Object&gt; hooks_ping

For 3rd party and custom integrations to test connectivity after authentication



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::HooksApi.new

begin
  #For 3rd party and custom integrations to test connectivity after authentication
  result = api_instance.hooks_ping
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling HooksApi->hooks_ping: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

**Array&lt;Object&gt;**

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **hooks_poll**
> Object hooks_poll(type, opts)

For 3rd party and custom integrations to view shape for `data` property of POST from Brushfire



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::HooksApi.new

type = 'type_example' # String | 

opts = { 
  event_id: 789 # Integer | 
}

begin
  #For 3rd party and custom integrations to view shape for `data` property of POST from Brushfire
  result = api_instance.hooks_poll(type, opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling HooksApi->hooks_poll: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **String**|  | 
 **event_id** | **Integer**|  | [optional] 

### Return type

**Object**

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **hooks_sample**
> Object hooks_sample(type, opts)

For 3rd party and custom integrations to view shape for `data` property of POST from Brushfire



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::HooksApi.new

type = 'type_example' # String | 

opts = { 
  event_id: 'event_id_example' # String | 
}

begin
  #For 3rd party and custom integrations to view shape for `data` property of POST from Brushfire
  result = api_instance.hooks_sample(type, opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling HooksApi->hooks_sample: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **String**|  | 
 **event_id** | **String**|  | [optional] 

### Return type

**Object**

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **hooks_subscribe**
> Object hooks_subscribe(body)

For 3rd party and custom integrations to subscribe to our notifications



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::HooksApi.new

body = Brushfire::HookSubscribeInput.new # HookSubscribeInput | 


begin
  #For 3rd party and custom integrations to subscribe to our notifications
  result = api_instance.hooks_subscribe(body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling HooksApi->hooks_subscribe: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**HookSubscribeInput**](HookSubscribeInput.md)|  | 

### Return type

**Object**

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **hooks_unsubscribe**
> BOOLEAN hooks_unsubscribe(body)

For 3rd party and custom integrations to unsubscribe from our notifications

### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::HooksApi.new

body = Brushfire::HookUnsubscribeInput.new # HookUnsubscribeInput | 


begin
  #For 3rd party and custom integrations to unsubscribe from our notifications
  result = api_instance.hooks_unsubscribe(body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling HooksApi->hooks_unsubscribe: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**HookUnsubscribeInput**](HookUnsubscribeInput.md)|  | 

### Return type

**BOOLEAN**

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



