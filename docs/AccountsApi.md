# Brushfire::AccountsApi

All URIs are relative to *https://api.brushfire.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accounts_auth**](AccountsApi.md#accounts_auth) | **POST** /accounts/auth | Authenticate a user via email and password or social service to get Access Key
[**accounts_auth_social**](AccountsApi.md#accounts_auth_social) | **POST** /accounts/authsocial | Authenticate a user via Facebook/Google/Twitter to get Access Key
[**accounts_helpdesk**](AccountsApi.md#accounts_helpdesk) | **GET** /accounts/helpdesk/{email} | Retrieve an account that matches the provided email
[**accounts_single**](AccountsApi.md#accounts_single) | **GET** /accounts/{accountId} | Retrieve information for a specific account


# **accounts_auth**
> AccountAuthOutput accounts_auth(body)

Authenticate a user via email and password or social service to get Access Key



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::AccountsApi.new

body = Brushfire::AccountAuthInput.new # AccountAuthInput | 


begin
  #Authenticate a user via email and password or social service to get Access Key
  result = api_instance.accounts_auth(body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling AccountsApi->accounts_auth: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AccountAuthInput**](AccountAuthInput.md)|  | 

### Return type

[**AccountAuthOutput**](AccountAuthOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **accounts_auth_social**
> AccountAuthOutput accounts_auth_social(body)

Authenticate a user via Facebook/Google/Twitter to get Access Key

Access is restricted to specific apps.

### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::AccountsApi.new

body = Brushfire::AccountAuthSocialInput.new # AccountAuthSocialInput | 


begin
  #Authenticate a user via Facebook/Google/Twitter to get Access Key
  result = api_instance.accounts_auth_social(body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling AccountsApi->accounts_auth_social: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AccountAuthSocialInput**](AccountAuthSocialInput.md)|  | 

### Return type

[**AccountAuthOutput**](AccountAuthOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **accounts_helpdesk**
> AccountHelpdeskOutput accounts_helpdesk(email)

Retrieve an account that matches the provided email



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::AccountsApi.new

email = 'email_example' # String | An Email that corresponds to a specific account


begin
  #Retrieve an account that matches the provided email
  result = api_instance.accounts_helpdesk(email)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling AccountsApi->accounts_helpdesk: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| An Email that corresponds to a specific account | 

### Return type

[**AccountHelpdeskOutput**](AccountHelpdeskOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **accounts_single**
> AccountSingleOutput accounts_single(account_id)

Retrieve information for a specific account



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::AccountsApi.new

account_id = 'account_id_example' # String | An Email, a GUID, an Access Key, or an integer that corresponds to a specific account


begin
  #Retrieve information for a specific account
  result = api_instance.accounts_single(account_id)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling AccountsApi->accounts_single: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **String**| An Email, a GUID, an Access Key, or an integer that corresponds to a specific account | 

### Return type

[**AccountSingleOutput**](AccountSingleOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



