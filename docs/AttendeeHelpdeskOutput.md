# Brushfire::AttendeeHelpdeskOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**attendee_number** | **Integer** |  | [optional] 
**event_number** | **Integer** |  | [optional] 
**event_title** | **String** |  | [optional] 
**event_subtitle** | **String** |  | [optional] 
**status** | **String** |  | [optional] 
**first_name** | **String** |  | [optional] 
**last_name** | **String** |  | [optional] 
**type_name** | **String** |  | [optional] 
**amount** | **Float** |  | [optional] 
**seat_info** | **String** |  | [optional] 
**culture** | **String** |  | [optional] 


