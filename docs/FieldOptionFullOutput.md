# Brushfire::FieldOptionFullOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**label** | **String** |  | [optional] 
**capacity** | **Integer** |  | [optional] 
**quantity** | **Integer** |  | [optional] 
**has_unit_price** | **BOOLEAN** |  | [optional] 
**unit_price** | **Float** |  | [optional] 
**is_selected** | **BOOLEAN** |  | [optional] 
**has_capacity** | **BOOLEAN** |  | [optional] 


