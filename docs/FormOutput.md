# Brushfire::FormOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**object_id** | **String** |  | [optional] 
**is_attendee** | **BOOLEAN** |  | [optional] 
**attendee_number** | **Integer** |  | [optional] 
**is_pre_registered** | **BOOLEAN** |  | [optional] 
**is_gift** | **BOOLEAN** |  | [optional] 
**share_email** | **String** |  | [optional] 
**copy_enabled** | **BOOLEAN** |  | [optional] 
**gifting_enabled** | **BOOLEAN** |  | [optional] 
**pre_reg_enabled** | **BOOLEAN** |  | [optional] 
**is_copy_source** | **BOOLEAN** |  | [optional] 
**is_copy_destination** | **BOOLEAN** |  | [optional] 
**can_continue** | **BOOLEAN** |  | [optional] 
**attendee_type_id** | **String** |  | [optional] 
**attendee_type_name** | **String** |  | [optional] 
**is_completed** | **BOOLEAN** |  | [optional] 
**seat_id** | **String** |  | [optional] 
**section_name** | **String** |  | [optional] 
**row_name** | **String** |  | [optional] 
**seat_label** | **String** |  | [optional] 
**unit_price** | **Float** |  | [optional] 
**unit_addons** | **Float** |  | [optional] 
**culture** | **String** |  | [optional] 
**copyable_attendee_numbers** | **Array&lt;Integer&gt;** |  | [optional] 
**display_name** | **String** |  | [optional] 
**attendee_display** | **Array&lt;String&gt;** |  | [optional] 
**fields** | [**Array&lt;FieldOutput&gt;**](FieldOutput.md) |  | [optional] 


