# Brushfire::CartAttendeeFormInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_key** | **String** |  | [optional] 
**field_values** | [**Array&lt;FieldInput&gt;**](FieldInput.md) |  | [optional] 
**update_new_only** | **BOOLEAN** |  | [optional] 
**is_pre_registered** | **BOOLEAN** |  | [optional] 
**is_gift** | **BOOLEAN** |  | [optional] 
**recipient_email** | **String** |  | [optional] 


