# Brushfire::CartPromotionOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**names** | **Array&lt;String&gt;** |  | [optional] 
**amount** | **Float** |  | [optional] 
**auto_calculate** | **BOOLEAN** |  | [optional] 


