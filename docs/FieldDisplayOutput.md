# Brushfire::FieldDisplayOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | **String** |  | [optional] 
**value** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**option_ids** | **Array&lt;String&gt;** |  | [optional] 


