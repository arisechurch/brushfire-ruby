# Brushfire::CartEventDeliveryMethodOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipping_id** | **String** |  | [optional] 
**method** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**event_fee** | **Float** |  | [optional] 
**ticket_fee** | **Float** |  | [optional] 
**is_electronic** | **BOOLEAN** |  | [optional] 
**is_point_of_sale** | **BOOLEAN** |  | [optional] 
**prices_vary** | **BOOLEAN** |  | [optional] 
**ticket_count** | **Integer** |  | [optional] 
**fee** | **Float** |  | [optional] 


