# Brushfire::OrderHelpdeskOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**order_key** | **String** |  | [optional] 
**object_number** | **Integer** |  | [optional] 
**object_title** | **String** |  | [optional] 
**object_subtitle** | **String** |  | [optional] 
**ordered_at** | **DateTime** |  | [optional] 
**ordered_at_local** | **DateTime** |  | [optional] 
**first_name** | **String** |  | [optional] 
**last_name** | **String** |  | [optional] 
**amount** | **Float** |  | [optional] 
**is_adjustment** | **BOOLEAN** |  | [optional] 
**culture** | **String** |  | [optional] 


