# Brushfire::OrderAttendeeSummaryOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attendee_id** | **String** |  | [optional] 
**attendee_number** | **Integer** |  | [optional] 
**attendee_code** | **String** |  | [optional] 
**has_name** | **BOOLEAN** |  | [optional] 
**first_name** | **String** |  | [optional] 
**last_name** | **String** |  | [optional] 
**section_name** | **String** |  | [optional] 
**row_name** | **String** |  | [optional] 
**seat_label** | **String** |  | [optional] 
**created_at** | **DateTime** |  | [optional] 
**type_id** | **String** |  | [optional] 
**type_name** | **String** |  | [optional] 
**group_id** | **String** |  | [optional] 
**group_name** | **String** |  | [optional] 
**order_quantity** | **Integer** |  | [optional] 
**status** | **String** |  | [optional] 
**is_completed** | **BOOLEAN** |  | [optional] 
**is_pre_registered** | **BOOLEAN** |  | [optional] 
**is_gift** | **BOOLEAN** |  | [optional] 
**share_email** | **String** |  | [optional] 
**gifting_enabled** | **BOOLEAN** |  | [optional] 
**gift_last_sent_at** | **DateTime** |  | [optional] 
**unit_price** | **Float** |  | [optional] 
**unit_discount** | **Float** |  | [optional] 
**unit_addons** | **Float** |  | [optional] 
**unit_fee** | **Float** |  | [optional] 
**unit_tax** | **Float** |  | [optional] 
**unit_delivery** | **Float** |  | [optional] 
**unit_total** | **Float** |  | [optional] 
**refundable** | **BOOLEAN** |  | [optional] 
**promotions** | [**Array&lt;OrderPromotionOutput&gt;**](OrderPromotionOutput.md) |  | [optional] 


