# Brushfire::ApiHttpError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **String** |  | [optional] 
**data** | **Object** |  | [optional] 


