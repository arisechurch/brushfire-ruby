# Brushfire::CartUpdateAttendeeInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_key** | **String** |  | [optional] 
**attendee_type_id** | **String** |  | [optional] 
**group_id** | **String** |  | [optional] 
**unit_price** | **Float** |  | [optional] 
**unit_addons** | **Float** |  | [optional] 
**unit_discount** | **Float** |  | [optional] 
**unit_tax** | **Float** |  | [optional] 
**unit_fee** | **Float** |  | [optional] 
**unit_delivery** | **Float** |  | [optional] 


