# Brushfire::PromotionsApi

All URIs are relative to *https://api.brushfire.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**promotions_create_dynamic**](PromotionsApi.md#promotions_create_dynamic) | **POST** /promotions/dynamic | Creates the specified quantity of dynamic promotions based upon the provided template code
[**promotions_list**](PromotionsApi.md#promotions_list) | **GET** /promotions | List all promotions for a specific client (and additional parameters)
[**promotions_single**](PromotionsApi.md#promotions_single) | **GET** /promotions/{promotionId} | Get details of a single promotion


# **promotions_create_dynamic**
> Array&lt;PromotionCreateOutput&gt; promotions_create_dynamic(body)

Creates the specified quantity of dynamic promotions based upon the provided template code



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::PromotionsApi.new

body = Brushfire::PromotionCreateInput.new # PromotionCreateInput | 


begin
  #Creates the specified quantity of dynamic promotions based upon the provided template code
  result = api_instance.promotions_create_dynamic(body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling PromotionsApi->promotions_create_dynamic: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PromotionCreateInput**](PromotionCreateInput.md)|  | 

### Return type

[**Array&lt;PromotionCreateOutput&gt;**](PromotionCreateOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **promotions_list**
> Array&lt;PromotionListOutput&gt; promotions_list(client_id, opts)

List all promotions for a specific client (and additional parameters)



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::PromotionsApi.new

client_id = 'client_id_example' # String | A string, a GUID, or an integer that corresponds to a specific client

opts = { 
  kind: 'kind_example', # String | An option string value from \"Standard\", \"Template\", or \"Dynamic\" that represents the kind of promotion
  is_redeemed: true, # BOOLEAN | An optional boolean value if the promotion has been redeemed
  search: 'search_example' # String | An option string to filter promotion codes by name
}

begin
  #List all promotions for a specific client (and additional parameters)
  result = api_instance.promotions_list(client_id, opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling PromotionsApi->promotions_list: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **String**| A string, a GUID, or an integer that corresponds to a specific client | 
 **kind** | **String**| An option string value from \&quot;Standard\&quot;, \&quot;Template\&quot;, or \&quot;Dynamic\&quot; that represents the kind of promotion | [optional] 
 **is_redeemed** | **BOOLEAN**| An optional boolean value if the promotion has been redeemed | [optional] 
 **search** | **String**| An option string to filter promotion codes by name | [optional] 

### Return type

[**Array&lt;PromotionListOutput&gt;**](PromotionListOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **promotions_single**
> PromotionSingleOutput promotions_single(promotion_id)

Get details of a single promotion



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::PromotionsApi.new

promotion_id = 'promotion_id_example' # String | A GUID that corresponds to a specific promotion


begin
  #Get details of a single promotion
  result = api_instance.promotions_single(promotion_id)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling PromotionsApi->promotions_single: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **promotion_id** | **String**| A GUID that corresponds to a specific promotion | 

### Return type

[**PromotionSingleOutput**](PromotionSingleOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



