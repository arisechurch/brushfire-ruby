# Brushfire::SessionCheckinMultipleInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**codes** | **Array&lt;String&gt;** |  | [optional] 
**entries** | [**Array&lt;SessionCheckinEntryItem&gt;**](SessionCheckinEntryItem.md) |  | [optional] 


