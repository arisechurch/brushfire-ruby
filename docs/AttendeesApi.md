# Brushfire::AttendeesApi

All URIs are relative to *https://api.brushfire.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**attendees_code**](AttendeesApi.md#attendees_code) | **POST** /attendees/{attendeeId}/code | Associate an external code with a Brushfire attendee for scanning, etc.
[**attendees_communities**](AttendeesApi.md#attendees_communities) | **GET** /attendees/{attendeeId}/communities | Gets all the available communities for this specific attendee based upon any rules that might be in place
[**attendees_data**](AttendeesApi.md#attendees_data) | **GET** /attendees/{attendeeId}/data | Returns all data for the specified attendee in the same structure as {eventId}/data
[**attendees_details**](AttendeesApi.md#attendees_details) | **GET** /attendees/{attendeeId} | Retrieve all relevant attendee information for the given attendee number
[**attendees_fields**](AttendeesApi.md#attendees_fields) | **GET** /attendees/{attendeeId}/fields | Retrieve fields for an attendee
[**attendees_helpdesk**](AttendeesApi.md#attendees_helpdesk) | **GET** /attendees/helpdesk/{email} | Retrieve attendees that match provided email
[**attendees_link**](AttendeesApi.md#attendees_link) | **POST** /attendees/{attendeeId}/link | Copy all fields from the source attendee to the specified attendee and link them together
[**attendees_print**](AttendeesApi.md#attendees_print) | **GET** /attendees/{attendeeId}/print | Retrieve the printable resources for a single attendee
[**attendees_print_all**](AttendeesApi.md#attendees_print_all) | **POST** /attendees/print | Retrieve the printable resources for multiple attendees
[**attendees_update**](AttendeesApi.md#attendees_update) | **POST** /attendees/{attendeeId}/fields | Update fields for an attendee
[**attendees_update_community**](AttendeesApi.md#attendees_update_community) | **POST** /attendees/{attendeeId}/community | Set or clear the community for the specified attendee (as long as the group&#39;s community doesn&#39;t override)
[**attendees_update_gift**](AttendeesApi.md#attendees_update_gift) | **POST** /attendees/{attendeeId}/gift | Set an attendee registration as a gift
[**attendees_update_group**](AttendeesApi.md#attendees_update_group) | **POST** /attendees/{attendeeId}/group | Set or clear the group (and the associated community) for the specified attendee


# **attendees_code**
> AttendeeDetailsOutput attendees_code(attendee_id, body)

Associate an external code with a Brushfire attendee for scanning, etc.



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::AttendeesApi.new

attendee_id = 'attendee_id_example' # String | An integer or a GUID that corresponds to a specific attendee

body = Brushfire::AttendeeCodeInput.new # AttendeeCodeInput | 


begin
  #Associate an external code with a Brushfire attendee for scanning, etc.
  result = api_instance.attendees_code(attendee_id, body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling AttendeesApi->attendees_code: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attendee_id** | **String**| An integer or a GUID that corresponds to a specific attendee | 
 **body** | [**AttendeeCodeInput**](AttendeeCodeInput.md)|  | 

### Return type

[**AttendeeDetailsOutput**](AttendeeDetailsOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **attendees_communities**
> Array&lt;AttendeeCommunityOutput&gt; attendees_communities(attendee_id, opts)

Gets all the available communities for this specific attendee based upon any rules that might be in place



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::AttendeesApi.new

attendee_id = 'attendee_id_example' # String | An integer or a GUID that corresponds to a specific attendee

opts = { 
  access_key: 'access_key_example' # String | The Access Key for the user context
}

begin
  #Gets all the available communities for this specific attendee based upon any rules that might be in place
  result = api_instance.attendees_communities(attendee_id, opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling AttendeesApi->attendees_communities: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attendee_id** | **String**| An integer or a GUID that corresponds to a specific attendee | 
 **access_key** | **String**| The Access Key for the user context | [optional] 

### Return type

[**Array&lt;AttendeeCommunityOutput&gt;**](AttendeeCommunityOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **attendees_data**
> Object attendees_data(attendee_id)

Returns all data for the specified attendee in the same structure as {eventId}/data



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::AttendeesApi.new

attendee_id = 'attendee_id_example' # String | An integer, GUID, or a string that corresponds to a specific attendee


begin
  #Returns all data for the specified attendee in the same structure as {eventId}/data
  result = api_instance.attendees_data(attendee_id)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling AttendeesApi->attendees_data: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attendee_id** | **String**| An integer, GUID, or a string that corresponds to a specific attendee | 

### Return type

**Object**

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **attendees_details**
> AttendeeDetailsOutput attendees_details(attendee_id, opts)

Retrieve all relevant attendee information for the given attendee number



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::AttendeesApi.new

attendee_id = 'attendee_id_example' # String | An integer, GUID, or a string that corresponds to a specific attendee

opts = { 
  event_id: 'event_id_example', # String | An integer or a GUID that corresponds to a specific event (only necessary if sending an attendee code)
  access_key: 'access_key_example', # String | The Access Key for the user context
  qr_size: 56 # Integer | The width and height of the QR Code. If blank, no QR will be sent.
}

begin
  #Retrieve all relevant attendee information for the given attendee number
  result = api_instance.attendees_details(attendee_id, opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling AttendeesApi->attendees_details: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attendee_id** | **String**| An integer, GUID, or a string that corresponds to a specific attendee | 
 **event_id** | **String**| An integer or a GUID that corresponds to a specific event (only necessary if sending an attendee code) | [optional] 
 **access_key** | **String**| The Access Key for the user context | [optional] 
 **qr_size** | **Integer**| The width and height of the QR Code. If blank, no QR will be sent. | [optional] 

### Return type

[**AttendeeDetailsOutput**](AttendeeDetailsOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **attendees_fields**
> Array&lt;FieldOutput&gt; attendees_fields(attendee_id, opts)

Retrieve fields for an attendee



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::AttendeesApi.new

attendee_id = 'attendee_id_example' # String | An integer or a GUID that corresponds to a specific attendee

opts = { 
  access_key: 'access_key_example' # String | The Access Key for the user context
}

begin
  #Retrieve fields for an attendee
  result = api_instance.attendees_fields(attendee_id, opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling AttendeesApi->attendees_fields: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attendee_id** | **String**| An integer or a GUID that corresponds to a specific attendee | 
 **access_key** | **String**| The Access Key for the user context | [optional] 

### Return type

[**Array&lt;FieldOutput&gt;**](FieldOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **attendees_helpdesk**
> Array&lt;AttendeeHelpdeskOutput&gt; attendees_helpdesk(email)

Retrieve attendees that match provided email



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::AttendeesApi.new

email = 'email_example' # String | An email to search across attendee email fields


begin
  #Retrieve attendees that match provided email
  result = api_instance.attendees_helpdesk(email)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling AttendeesApi->attendees_helpdesk: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| An email to search across attendee email fields | 

### Return type

[**Array&lt;AttendeeHelpdeskOutput&gt;**](AttendeeHelpdeskOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **attendees_link**
> AttendeeDetailsOutput attendees_link(attendee_id, body)

Copy all fields from the source attendee to the specified attendee and link them together



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::AttendeesApi.new

attendee_id = 'attendee_id_example' # String | An integer or a GUID that corresponds to a specific attendee

body = Brushfire::AttendeeLinkInput.new # AttendeeLinkInput | 


begin
  #Copy all fields from the source attendee to the specified attendee and link them together
  result = api_instance.attendees_link(attendee_id, body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling AttendeesApi->attendees_link: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attendee_id** | **String**| An integer or a GUID that corresponds to a specific attendee | 
 **body** | [**AttendeeLinkInput**](AttendeeLinkInput.md)|  | 

### Return type

[**AttendeeDetailsOutput**](AttendeeDetailsOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **attendees_print**
> String attendees_print(attendee_id, type)

Retrieve the printable resources for a single attendee



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::AttendeesApi.new

attendee_id = 'attendee_id_example' # String | An integer or a GUID that corresponds to a specific attendee

type = 'type_example' # String | A string that is one of \"Ticket\", \"ETicket\", or \"Namebadge\"


begin
  #Retrieve the printable resources for a single attendee
  result = api_instance.attendees_print(attendee_id, type)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling AttendeesApi->attendees_print: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attendee_id** | **String**| An integer or a GUID that corresponds to a specific attendee | 
 **type** | **String**| A string that is one of \&quot;Ticket\&quot;, \&quot;ETicket\&quot;, or \&quot;Namebadge\&quot; | 

### Return type

**String**

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **attendees_print_all**
> String attendees_print_all(body)

Retrieve the printable resources for multiple attendees



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::AttendeesApi.new

body = Brushfire::AttendeePrintInput.new # AttendeePrintInput | 


begin
  #Retrieve the printable resources for multiple attendees
  result = api_instance.attendees_print_all(body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling AttendeesApi->attendees_print_all: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AttendeePrintInput**](AttendeePrintInput.md)|  | 

### Return type

**String**

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **attendees_update**
> Array&lt;FieldOutput&gt; attendees_update(attendee_id, body)

Update fields for an attendee



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::AttendeesApi.new

attendee_id = 'attendee_id_example' # String | An integer or a GUID that corresponds to a specific attendee

body = Brushfire::FieldUpdateInput.new # FieldUpdateInput | 


begin
  #Update fields for an attendee
  result = api_instance.attendees_update(attendee_id, body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling AttendeesApi->attendees_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attendee_id** | **String**| An integer or a GUID that corresponds to a specific attendee | 
 **body** | [**FieldUpdateInput**](FieldUpdateInput.md)|  | 

### Return type

[**Array&lt;FieldOutput&gt;**](FieldOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **attendees_update_community**
> AttendeeDetailsOutput attendees_update_community(attendee_id, body)

Set or clear the community for the specified attendee (as long as the group's community doesn't override)



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::AttendeesApi.new

attendee_id = 'attendee_id_example' # String | An integer or a GUID that corresponds to a specific attendee

body = Brushfire::AttendeeCommunityInput.new # AttendeeCommunityInput | 


begin
  #Set or clear the community for the specified attendee (as long as the group's community doesn't override)
  result = api_instance.attendees_update_community(attendee_id, body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling AttendeesApi->attendees_update_community: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attendee_id** | **String**| An integer or a GUID that corresponds to a specific attendee | 
 **body** | [**AttendeeCommunityInput**](AttendeeCommunityInput.md)|  | 

### Return type

[**AttendeeDetailsOutput**](AttendeeDetailsOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **attendees_update_gift**
> AttendeeUpdateGiftOutput attendees_update_gift(attendee_id, body)

Set an attendee registration as a gift



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::AttendeesApi.new

attendee_id = 'attendee_id_example' # String | An integer or a GUID that corresponds to a specific attendee

body = Brushfire::AttendeeUpdateGiftInput.new # AttendeeUpdateGiftInput | 


begin
  #Set an attendee registration as a gift
  result = api_instance.attendees_update_gift(attendee_id, body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling AttendeesApi->attendees_update_gift: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attendee_id** | **String**| An integer or a GUID that corresponds to a specific attendee | 
 **body** | [**AttendeeUpdateGiftInput**](AttendeeUpdateGiftInput.md)|  | 

### Return type

[**AttendeeUpdateGiftOutput**](AttendeeUpdateGiftOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **attendees_update_group**
> AttendeeDetailsOutput attendees_update_group(attendee_id, body)

Set or clear the group (and the associated community) for the specified attendee



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::AttendeesApi.new

attendee_id = 'attendee_id_example' # String | An integer or a GUID that corresponds to a specific attendee

body = Brushfire::AttendeeGroupInput.new # AttendeeGroupInput | 


begin
  #Set or clear the group (and the associated community) for the specified attendee
  result = api_instance.attendees_update_group(attendee_id, body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling AttendeesApi->attendees_update_group: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attendee_id** | **String**| An integer or a GUID that corresponds to a specific attendee | 
 **body** | [**AttendeeGroupInput**](AttendeeGroupInput.md)|  | 

### Return type

[**AttendeeDetailsOutput**](AttendeeDetailsOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



