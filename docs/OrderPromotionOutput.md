# Brushfire::OrderPromotionOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**promotion_id** | **String** |  | [optional] 
**discount_amount** | **Float** |  | [optional] 


