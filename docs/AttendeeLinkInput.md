# Brushfire::AttendeeLinkInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source_attendee_id** | **String** |  | [optional] 
**access_key** | **String** |  | [optional] 


