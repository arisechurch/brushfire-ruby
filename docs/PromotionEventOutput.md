# Brushfire::PromotionEventOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_id** | **String** |  | [optional] 
**attendee_types** | **Array&lt;String&gt;** |  | [optional] 


