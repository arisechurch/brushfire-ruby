# Brushfire::OrderCreateInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cart_id** | **String** |  | 
**access_key** | **String** |  | [optional] 
**customer_account_number** | **Integer** |  | [optional] 
**billing_first_name** | **String** |  | 
**billing_last_name** | **String** |  | 
**billing_organization** | **String** |  | [optional] 
**billing_street1** | **String** |  | 
**billing_street2** | **String** |  | [optional] 
**billing_city** | **String** |  | 
**billing_region** | **String** |  | 
**billing_country** | **String** |  | 
**billing_postal_code** | **String** |  | 
**contact_email** | **String** |  | [optional] 
**contact_phone** | **String** |  | 
**shipping_billing_same** | **BOOLEAN** |  | [optional] 
**shipping_first_name** | **String** |  | [optional] 
**shipping_last_name** | **String** |  | [optional] 
**shipping_organization** | **String** |  | [optional] 
**shipping_street1** | **String** |  | [optional] 
**shipping_street2** | **String** |  | [optional] 
**shipping_city** | **String** |  | [optional] 
**shipping_region** | **String** |  | [optional] 
**shipping_country** | **String** |  | [optional] 
**shipping_postal_code** | **String** |  | [optional] 
**payment_method_id** | **String** |  | [optional] 
**transaction_id** | **String** |  | [optional] 
**token** | **String** |  | [optional] 
**track_data** | **String** |  | [optional] 
**card_name** | **String** |  | [optional] 
**card_number** | **String** |  | [optional] 
**card_csc** | **String** |  | [optional] 
**card_month** | **Integer** |  | [optional] 
**card_year** | **Integer** |  | [optional] 
**routing_number** | **String** |  | [optional] 
**account_number** | **String** |  | [optional] 
**check_po_number** | **String** |  | [optional] 
**paid_in_full** | **BOOLEAN** |  | [optional] 
**payment_number_for_storage** | **String** |  | [optional] 
**notes** | **String** |  | [optional] 
**is_kiosk** | **BOOLEAN** |  | [optional] 
**via** | **String** |  | [optional] 


