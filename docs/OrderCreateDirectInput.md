# Brushfire::OrderCreateDirectInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_key** | **String** |  | [optional] 
**first_name** | **String** |  | 
**last_name** | **String** |  | 
**organization** | **String** |  | [optional] 
**street1** | **String** |  | [optional] 
**street2** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**region** | **String** |  | [optional] 
**country** | **String** |  | [optional] 
**postal_code** | **String** |  | [optional] 
**phone** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**is_pos** | **BOOLEAN** |  | [optional] 
**event_number** | **Integer** |  | 
**number_of_tickets** | **Integer** |  | 
**notes** | **String** |  | [optional] 
**send_email** | **BOOLEAN** |  | [optional] 
**ship_method** | **String** |  | [optional] 


