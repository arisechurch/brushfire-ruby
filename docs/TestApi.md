# Brushfire::TestApi

All URIs are relative to *https://api.brushfire.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**test_info**](TestApi.md#test_info) | **GET** /test/info | 


# **test_info**
> Array&lt;PersonalInfoDataItem&gt; test_info(event_id)



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::TestApi.new

event_id = 'event_id_example' # String | 


begin
  result = api_instance.test_info(event_id)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling TestApi->test_info: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_id** | **String**|  | 

### Return type

[**Array&lt;PersonalInfoDataItem&gt;**](PersonalInfoDataItem.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



