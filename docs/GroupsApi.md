# Brushfire::GroupsApi

All URIs are relative to *https://api.brushfire.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**groups_create**](GroupsApi.md#groups_create) | **POST** /groups | Create a group
[**groups_fields**](GroupsApi.md#groups_fields) | **GET** /groups/{groupId}/fields | Retrieve fields for a group
[**groups_fields_update**](GroupsApi.md#groups_fields_update) | **POST** /groups/{groupId}/fields | Update fields for a group
[**groups_helpdesk**](GroupsApi.md#groups_helpdesk) | **GET** /groups/helpdesk/{email} | Retrieve groups that match provided email
[**groups_print**](GroupsApi.md#groups_print) | **GET** /groups/{groupId}/print | Retrieve the printable resources for a single group
[**groups_print_all**](GroupsApi.md#groups_print_all) | **POST** /groups/print | Retrieve the printable resources for multiple groups
[**groups_single**](GroupsApi.md#groups_single) | **GET** /groups/{groupId} | Retrieve information about a group
[**groups_update**](GroupsApi.md#groups_update) | **PUT** /groups | Update a group
[**groups_update_community**](GroupsApi.md#groups_update_community) | **POST** /groups/{groupId}/community | Set or clear the community for the specified group and all of its attendees


# **groups_create**
> GroupCreateOutput groups_create(body)

Create a group



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::GroupsApi.new

body = Brushfire::GroupCreateInput.new # GroupCreateInput | 


begin
  #Create a group
  result = api_instance.groups_create(body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling GroupsApi->groups_create: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**GroupCreateInput**](GroupCreateInput.md)|  | 

### Return type

[**GroupCreateOutput**](GroupCreateOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **groups_fields**
> Array&lt;FieldOutput&gt; groups_fields(group_id, opts)

Retrieve fields for a group



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::GroupsApi.new

group_id = 'group_id_example' # String | A GUID or an integer representing a group

opts = { 
  access_key: 'access_key_example' # String | The Access Key for the user context
}

begin
  #Retrieve fields for a group
  result = api_instance.groups_fields(group_id, opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling GroupsApi->groups_fields: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **String**| A GUID or an integer representing a group | 
 **access_key** | **String**| The Access Key for the user context | [optional] 

### Return type

[**Array&lt;FieldOutput&gt;**](FieldOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **groups_fields_update**
> Array&lt;FieldOutput&gt; groups_fields_update(group_id, body)

Update fields for a group



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::GroupsApi.new

group_id = 'group_id_example' # String | A GUID or an integer representing a group

body = Brushfire::FieldUpdateInput.new # FieldUpdateInput | 


begin
  #Update fields for a group
  result = api_instance.groups_fields_update(group_id, body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling GroupsApi->groups_fields_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **String**| A GUID or an integer representing a group | 
 **body** | [**FieldUpdateInput**](FieldUpdateInput.md)|  | 

### Return type

[**Array&lt;FieldOutput&gt;**](FieldOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **groups_helpdesk**
> Array&lt;GroupHelpdeskOutput&gt; groups_helpdesk(email)

Retrieve groups that match provided email



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::GroupsApi.new

email = 'email_example' # String | An email to search across group email fields


begin
  #Retrieve groups that match provided email
  result = api_instance.groups_helpdesk(email)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling GroupsApi->groups_helpdesk: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| An email to search across group email fields | 

### Return type

[**Array&lt;GroupHelpdeskOutput&gt;**](GroupHelpdeskOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **groups_print**
> String groups_print(group_id, type)

Retrieve the printable resources for a single group



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::GroupsApi.new

group_id = 'group_id_example' # String | A GUID or an integer representing a group

type = 'type_example' # String | A string that is one of \"Ticket\", \"ETicket\", or \"Namebadge\"


begin
  #Retrieve the printable resources for a single group
  result = api_instance.groups_print(group_id, type)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling GroupsApi->groups_print: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **String**| A GUID or an integer representing a group | 
 **type** | **String**| A string that is one of \&quot;Ticket\&quot;, \&quot;ETicket\&quot;, or \&quot;Namebadge\&quot; | 

### Return type

**String**

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **groups_print_all**
> String groups_print_all(body)

Retrieve the printable resources for multiple groups



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::GroupsApi.new

body = Brushfire::GroupPrintInput.new # GroupPrintInput | 


begin
  #Retrieve the printable resources for multiple groups
  result = api_instance.groups_print_all(body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling GroupsApi->groups_print_all: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**GroupPrintInput**](GroupPrintInput.md)|  | 

### Return type

**String**

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **groups_single**
> GroupSingleOutput groups_single(group_id, opts)

Retrieve information about a group



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::GroupsApi.new

group_id = 'group_id_example' # String | A GUID or an integer representing a group

opts = { 
  access_key: 'access_key_example', # String | The Access Key for the user context
  qr_size: 56 # Integer | The width and height of the QR Code. If blank, no QR will be sent.
}

begin
  #Retrieve information about a group
  result = api_instance.groups_single(group_id, opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling GroupsApi->groups_single: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **String**| A GUID or an integer representing a group | 
 **access_key** | **String**| The Access Key for the user context | [optional] 
 **qr_size** | **Integer**| The width and height of the QR Code. If blank, no QR will be sent. | [optional] 

### Return type

[**GroupSingleOutput**](GroupSingleOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **groups_update**
> GroupCreateOutput groups_update(body)

Update a group



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::GroupsApi.new

body = Brushfire::GroupUpdateInput.new # GroupUpdateInput | 


begin
  #Update a group
  result = api_instance.groups_update(body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling GroupsApi->groups_update: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**GroupUpdateInput**](GroupUpdateInput.md)|  | 

### Return type

[**GroupCreateOutput**](GroupCreateOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



# **groups_update_community**
> GroupCreateOutput groups_update_community(group_id, body)

Set or clear the community for the specified group and all of its attendees



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::GroupsApi.new

group_id = 'group_id_example' # String | A GUID or an integer representing a group

body = Brushfire::GroupCommunityInput.new # GroupCommunityInput | 


begin
  #Set or clear the community for the specified group and all of its attendees
  result = api_instance.groups_update_community(group_id, body)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling GroupsApi->groups_update_community: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group_id** | **String**| A GUID or an integer representing a group | 
 **body** | [**GroupCommunityInput**](GroupCommunityInput.md)|  | 

### Return type

[**GroupCreateOutput**](GroupCreateOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml



