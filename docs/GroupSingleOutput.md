# Brushfire::GroupSingleOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**group_number** | **Integer** |  | [optional] 
**event_number** | **Integer** |  | [optional] 
**event_id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**attendee_type_id** | **String** |  | [optional] 
**manage_code** | **String** |  | [optional] 
**join_code** | **String** |  | [optional] 
**type_name** | **String** |  | [optional] 
**attendee_count** | **Integer** |  | [optional] 
**community_id** | **String** |  | [optional] 
**community_partition** | **String** |  | [optional] 
**community_name** | **String** |  | [optional] 
**fields** | [**Array&lt;FieldOutput&gt;**](FieldOutput.md) |  | [optional] 
**attendees** | [**Array&lt;GroupAttendeeSummaryOutput&gt;**](GroupAttendeeSummaryOutput.md) |  | [optional] 
**qr_code** | **String** |  | [optional] 


