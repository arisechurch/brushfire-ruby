# Brushfire::ClientsApi

All URIs are relative to *https://api.brushfire.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**clients_list**](ClientsApi.md#clients_list) | **GET** /clients | Get a list of all clients available to this application
[**clients_single**](ClientsApi.md#clients_single) | **GET** /clients/{clientId} | Get the details for a specific client


# **clients_list**
> Array&lt;ClientListOutput&gt; clients_list

Get a list of all clients available to this application



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::ClientsApi.new

begin
  #Get a list of all clients available to this application
  result = api_instance.clients_list
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling ClientsApi->clients_list: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Array&lt;ClientListOutput&gt;**](ClientListOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



# **clients_single**
> ClientSingleOutput clients_single(client_id)

Get the details for a specific client



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::ClientsApi.new

client_id = 'client_id_example' # String | An string, a GUID, or an integer that corresponds to a specific client


begin
  #Get the details for a specific client
  result = api_instance.clients_single(client_id)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling ClientsApi->clients_single: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **String**| An string, a GUID, or an integer that corresponds to a specific client | 

### Return type

[**ClientSingleOutput**](ClientSingleOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



