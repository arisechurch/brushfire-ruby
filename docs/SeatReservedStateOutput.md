# Brushfire::SeatReservedStateOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**name** | **String** |  | [optional] 
**back_color** | [**ColorOutput**](ColorOutput.md) |  | [optional] 
**fore_color** | [**ColorOutput**](ColorOutput.md) |  | [optional] 


