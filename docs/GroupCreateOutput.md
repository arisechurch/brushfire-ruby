# Brushfire::GroupCreateOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**group_number** | **Integer** |  | [optional] 
**name** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**attendee_type_id** | **String** |  | [optional] 
**manage_code** | **String** |  | [optional] 
**join_code** | **String** |  | [optional] 
**type_name** | **String** |  | [optional] 
**community_id** | **String** |  | [optional] 
**community_partition** | **String** |  | [optional] 
**community_name** | **String** |  | [optional] 
**attendee_count** | **Integer** |  | [optional] 
**fields** | [**Array&lt;FieldOutput&gt;**](FieldOutput.md) |  | [optional] 


