# Brushfire::GroupCreateInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attendee_type_id** | **String** |  | 
**name** | **String** |  | 
**email** | **String** |  | 
**manage_code** | **String** |  | 
**join_code** | **String** |  | 
**access_key** | **String** |  | [optional] 
**community_id** | **String** |  | [optional] 
**field_values** | [**Array&lt;FieldInput&gt;**](FieldInput.md) |  | [optional] 


