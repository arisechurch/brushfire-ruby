# Brushfire::EventTypeOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**is_available** | **BOOLEAN** |  | [optional] 
**is_future** | **BOOLEAN** |  | [optional] 
**is_past** | **BOOLEAN** |  | [optional] 
**available** | **String** |  | [optional] 
**selected_amount** | **Float** |  | [optional] 
**amount** | **Float** |  | [optional] 
**fee** | **Float** |  | [optional] 
**allow_custom_amount** | **BOOLEAN** |  | [optional] 
**pre_reg_enabled** | **BOOLEAN** |  | [optional] 
**gifting_enabled** | **BOOLEAN** |  | [optional] 
**copy_enabled** | **BOOLEAN** |  | [optional] 
**payments_due_by** | **String** |  | [optional] 
**min_quantity** | **Integer** |  | [optional] 
**max_quantity** | **Integer** |  | [optional] 
**is_group** | **BOOLEAN** |  | [optional] 
**sold_out** | **BOOLEAN** |  | [optional] 
**your_group** | **BOOLEAN** |  | [optional] 
**grouping_text** | **String** |  | [optional] 
**is_inaccessible_due_to_access_code** | **BOOLEAN** |  | [optional] 
**is_accessible_because_of_access_code** | **BOOLEAN** |  | [optional] 
**is_admin** | **BOOLEAN** |  | [optional] 
**kind** | **String** |  | [optional] 
**allowed_payments** | **Array&lt;Float&gt;** |  | [optional] 
**sections** | [**Array&lt;EventTypeSectionOutput&gt;**](EventTypeSectionOutput.md) |  | [optional] 
**prices** | [**Array&lt;EventTypePriceOutput&gt;**](EventTypePriceOutput.md) |  | [optional] 


