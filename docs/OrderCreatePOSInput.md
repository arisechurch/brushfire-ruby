# Brushfire::OrderCreatePOSInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cart_id** | **String** |  | 
**access_key** | **String** |  | [optional] 
**billing_first_name** | **String** |  | 
**billing_last_name** | **String** |  | 
**billing_postal_code** | **String** |  | [optional] 
**contact_email** | **String** |  | [optional] 
**contact_phone** | **String** |  | [optional] 
**payment_method_id** | **String** |  | [optional] 
**transaction_id** | **String** |  | [optional] 
**token** | **String** |  | [optional] 
**track_data** | **String** |  | [optional] 
**card_name** | **String** |  | [optional] 
**card_number** | **String** |  | [optional] 
**card_csc** | **String** |  | [optional] 
**card_month** | **Integer** |  | [optional] 
**card_year** | **Integer** |  | [optional] 
**routing_number** | **String** |  | [optional] 
**account_number** | **String** |  | [optional] 
**check_po_number** | **String** |  | [optional] 
**paid_in_full** | **BOOLEAN** |  | [optional] 
**payment_number_for_storage** | **String** |  | [optional] 
**notes** | **String** |  | [optional] 
**is_kiosk** | **BOOLEAN** |  | [optional] 
**override_delivery_check** | **BOOLEAN** |  | [optional] 
**via** | **String** |  | [optional] 


