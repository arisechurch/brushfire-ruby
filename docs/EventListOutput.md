# Brushfire::EventListOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** |  | [optional] 
**alternate_title** | **String** |  | [optional] 
**area_name** | **String** |  | [optional] 
**area_key** | **String** |  | [optional] 
**client_name** | **String** |  | [optional] 
**date_info** | **String** |  | [optional] 
**event_number** | **Integer** |  | [optional] 
**is_assigned** | **BOOLEAN** |  | [optional] 
**location_name** | **String** |  | [optional] 
**session_count** | **Integer** |  | [optional] 
**is_active** | **BOOLEAN** |  | [optional] 
**time_zone_id** | **String** |  | [optional] 
**date_display** | [**DateDisplayOutput**](DateDisplayOutput.md) |  | [optional] 
**spots_remaining** | **Integer** |  | [optional] 
**spots_taken** | **Integer** |  | [optional] 
**dates** | [**Array&lt;EventDateOutput&gt;**](EventDateOutput.md) |  | [optional] 


