# Brushfire::EventLookupOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attendee_number** | **Integer** |  | [optional] 
**first_name** | **String** |  | [optional] 
**last_name** | **String** |  | [optional] 
**section_name** | **String** |  | [optional] 
**row_name** | **String** |  | [optional] 
**seat_label** | **String** |  | [optional] 
**type_name** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**phone** | **String** |  | [optional] 
**address** | **String** |  | [optional] 


