# Brushfire::ColorOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rgb** | **String** |  | [optional] 
**hex** | **String** |  | [optional] 


