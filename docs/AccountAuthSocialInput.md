# Brushfire::AccountAuthSocialInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**service_name** | **String** |  | 
**service_identifier** | **String** |  | 


