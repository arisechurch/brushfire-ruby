# Brushfire::AccountHelpdeskOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**account_number** | **Integer** |  | [optional] 
**email** | **String** |  | [optional] 
**first_name** | **String** |  | [optional] 
**last_name** | **String** |  | [optional] 
**is_user** | **BOOLEAN** |  | [optional] 
**client_roles** | [**Array&lt;AccountRoleOutput&gt;**](AccountRoleOutput.md) |  | [optional] 


