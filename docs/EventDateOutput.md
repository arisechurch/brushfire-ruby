# Brushfire::EventDateOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**doors_at** | **DateTime** |  | [optional] 
**starts_at** | **DateTime** |  | [optional] 
**ends_at** | **DateTime** |  | [optional] 


