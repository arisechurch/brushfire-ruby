# Brushfire::EventSeatOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seat_id** | **String** |  | [optional] 
**section_id** | **String** |  | [optional] 
**section_name** | **String** |  | [optional] 
**event_id** | **String** |  | [optional] 
**event_number** | **Integer** |  | [optional] 
**row_name** | **String** |  | [optional] 
**seat_number** | **Integer** |  | [optional] 
**seat_label** | **String** |  | [optional] 
**seat_type** | **String** |  | [optional] 
**wheelchair** | **BOOLEAN** |  | [optional] 
**limited_view** | **BOOLEAN** |  | [optional] 
**unit_price** | **Float** |  | [optional] 
**price_color** | [**ColorOutput**](ColorOutput.md) |  | [optional] 
**available** | **BOOLEAN** |  | [optional] 
**selected** | **BOOLEAN** |  | [optional] 
**reserved_at** | **DateTime** |  | [optional] 
**notes** | **String** |  | [optional] 
**order_id** | **String** |  | [optional] 
**order_key** | **String** |  | [optional] 
**attendee_id** | **String** |  | [optional] 
**attendee_number** | **Integer** |  | [optional] 
**state** | [**SeatReservedStateOutput**](SeatReservedStateOutput.md) |  | [optional] 
**is_inaccessible_due_to_access_code** | **BOOLEAN** |  | [optional] 
**types** | **Array&lt;String&gt;** |  | [optional] 
**checkins** | [**Array&lt;AttendeeCheckinOutput&gt;**](AttendeeCheckinOutput.md) |  | [optional] 


