# Brushfire::AttendeeGroupInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**group_id** | **String** |  | [optional] 
**override_community_capacity** | **BOOLEAN** |  | [optional] 
**access_key** | **String** |  | [optional] 


