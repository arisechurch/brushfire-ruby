# Brushfire::AttendeeCodeInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attendee_code** | **String** |  | [optional] 
**access_key** | **String** |  | [optional] 


