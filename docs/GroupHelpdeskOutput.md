# Brushfire::GroupHelpdeskOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**group_number** | **Integer** |  | [optional] 
**name** | **String** |  | [optional] 
**join_code** | **String** |  | [optional] 
**manage_code** | **String** |  | [optional] 
**attendee_count** | **Integer** |  | [optional] 
**event_number** | **Integer** |  | [optional] 
**event_title** | **String** |  | [optional] 
**event_subtitle** | **String** |  | [optional] 


