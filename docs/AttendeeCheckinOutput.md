# Brushfire::AttendeeCheckinOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**session_number** | **Integer** |  | [optional] 
**checked_in_at** | **DateTime** |  | [optional] 
**checked_in** | **BOOLEAN** |  | [optional] 


