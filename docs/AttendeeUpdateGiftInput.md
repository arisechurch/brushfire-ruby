# Brushfire::AttendeeUpdateGiftInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_gift** | **BOOLEAN** |  | [optional] 
**from_name** | **String** |  | [optional] 
**from_email** | **String** |  | [optional] 
**recipient_email** | **String** |  | [optional] 
**recipient_message** | **String** |  | [optional] 


