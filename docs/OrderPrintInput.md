# Brushfire::OrderPrintInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | 
**orders** | **Array&lt;String&gt;** |  | 


