# Brushfire::EventDocumentOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**has_ticket** | **BOOLEAN** |  | [optional] 
**has_e_ticket** | **BOOLEAN** |  | [optional] 
**has_receipt** | **BOOLEAN** |  | [optional] 
**has_namebadge** | **BOOLEAN** |  | [optional] 
**has_check_in** | **BOOLEAN** |  | [optional] 
**has_will_call_label** | **BOOLEAN** |  | [optional] 
**has_mail_label** | **BOOLEAN** |  | [optional] 


