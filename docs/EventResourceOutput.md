# Brushfire::EventResourceOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**url** | **String** |  | [optional] 


