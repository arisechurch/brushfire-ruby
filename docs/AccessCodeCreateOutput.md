# Brushfire::AccessCodeCreateOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**starts_at** | **DateTime** |  | [optional] 
**ends_at** | **DateTime** |  | [optional] 
**kind** | **String** |  | [optional] 
**is_single_use** | **BOOLEAN** |  | [optional] 
**is_redeemed** | **BOOLEAN** |  | [optional] 
**names** | **Array&lt;String&gt;** |  | [optional] 
**events** | **Array&lt;String&gt;** |  | [optional] 
**attendee_types** | **Array&lt;String&gt;** |  | [optional] 
**seats** | **Array&lt;String&gt;** |  | [optional] 


