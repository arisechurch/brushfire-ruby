# Brushfire::HookSubscribeInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  | 
**type** | **String** |  | 
**service** | **String** |  | [optional] 
**objects** | **Array&lt;Integer&gt;** |  | [optional] 


