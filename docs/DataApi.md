# Brushfire::DataApi

All URIs are relative to *https://api.brushfire.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**data_data**](DataApi.md#data_data) | **GET** /data/{id} | Returns specialized data from custom-written queries


# **data_data**
> Object data_data(id, opts)

Returns specialized data from custom-written queries

### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::DataApi.new

id = 'id_example' # String | The GUID for the query you are running

opts = { 
  granularity: 'granularity_example', # String | If present, one of the following string values \"hour\", \"minute\", or \"day\"
  starts_at: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | If present, the date/time in UTC to start data
  ends_at: DateTime.parse('2013-10-20T19:20:30+01:00') # DateTime | If present, the date/time in UTC to end data
}

begin
  #Returns specialized data from custom-written queries
  result = api_instance.data_data(id, opts)
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling DataApi->data_data: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| The GUID for the query you are running | 
 **granularity** | **String**| If present, one of the following string values \&quot;hour\&quot;, \&quot;minute\&quot;, or \&quot;day\&quot; | [optional] 
 **starts_at** | **DateTime**| If present, the date/time in UTC to start data | [optional] 
 **ends_at** | **DateTime**| If present, the date/time in UTC to end data | [optional] 

### Return type

**Object**

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



