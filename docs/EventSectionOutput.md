# Brushfire::EventSectionOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**section_id** | **String** |  | [optional] 
**event_id** | **String** |  | [optional] 
**event_number** | **Integer** |  | [optional] 
**name** | **String** |  | [optional] 
**alignment** | **String** |  | [optional] 
**aisles_after** | **Array&lt;String&gt;** |  | [optional] 
**is_row_ascending** | **BOOLEAN** |  | [optional] 
**is_seat_ascending** | **BOOLEAN** |  | [optional] 
**is_rank_ascending** | **BOOLEAN** |  | [optional] 
**is_assigned** | **BOOLEAN** |  | [optional] 
**rotation** | **Integer** |  | [optional] 
**rank** | **Integer** |  | [optional] 
**total_seat_count** | **Integer** |  | [optional] 
**available_spots** | **Integer** |  | [optional] 
**prices** | [**Array&lt;EventSectionPriceOutput&gt;**](EventSectionPriceOutput.md) |  | [optional] 
**reserved_states** | [**Array&lt;EventSectionReservedStateOutput&gt;**](EventSectionReservedStateOutput.md) |  | [optional] 


