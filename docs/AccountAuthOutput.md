# Brushfire::AccountAuthOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**first_name** | **String** |  | [optional] 
**last_name** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**account_number** | **Integer** |  | [optional] 
**access_key** | **String** |  | [optional] 
**is_user** | **BOOLEAN** |  | [optional] 
**avatar_url** | **String** |  | [optional] 


