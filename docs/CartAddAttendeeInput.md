# Brushfire::CartAddAttendeeInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attendees** | [**Array&lt;CartAddAttendeeItemInput&gt;**](CartAddAttendeeItemInput.md) |  | 
**access_key** | **String** |  | [optional] 


