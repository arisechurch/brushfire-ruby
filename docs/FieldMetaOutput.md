# Brushfire::FieldMetaOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field_id** | **String** |  | [optional] 
**field_name** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**field_type** | **String** |  | [optional] 


