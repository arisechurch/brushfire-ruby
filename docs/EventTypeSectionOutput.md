# Brushfire::EventTypeSectionOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**section_id** | **String** |  | [optional] 
**section_name** | **String** |  | [optional] 


