# Brushfire::AttendeeCommunityInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**community_id** | **String** |  | [optional] 
**access_key** | **String** |  | [optional] 
**override_capacity** | **BOOLEAN** |  | [optional] 
**override_group** | **BOOLEAN** |  | [optional] 


