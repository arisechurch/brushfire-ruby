# Brushfire::AttendeeCommunityOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**partition** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**capacity** | **Integer** |  | [optional] 
**attendee_count** | **Integer** |  | [optional] 
**group_count** | **Integer** |  | [optional] 
**tags** | **Array&lt;String&gt;** |  | [optional] 
**remaining_count** | **Integer** |  | [optional] 


