# Brushfire::PromotionSingleOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**starts_at** | **DateTime** |  | [optional] 
**ends_at** | **DateTime** |  | [optional] 
**group** | **String** |  | [optional] 
**kind** | **String** |  | [optional] 
**is_single_use** | **BOOLEAN** |  | [optional] 
**is_redeemed** | **BOOLEAN** |  | [optional] 
**admin_visible** | **BOOLEAN** |  | [optional] 
**internet_visible** | **BOOLEAN** |  | [optional] 
**amount** | **Float** |  | [optional] 
**is_percentage** | **BOOLEAN** |  | [optional] 
**auto_calculate** | **BOOLEAN** |  | [optional] 
**quantity** | **Integer** |  | [optional] 
**limit** | **Integer** |  | [optional] 
**exclude** | **Integer** |  | [optional] 
**is_multiple** | **BOOLEAN** |  | [optional] 
**span_children** | **BOOLEAN** |  | [optional] 
**in_each_child** | **BOOLEAN** |  | [optional] 
**include_addons** | **BOOLEAN** |  | [optional] 
**all_in_group** | **BOOLEAN** |  | [optional] 
**names** | **Array&lt;String&gt;** |  | [optional] 
**related_promotions** | **Array&lt;String&gt;** |  | [optional] 
**events** | [**Array&lt;PromotionEventOutput&gt;**](PromotionEventOutput.md) |  | [optional] 


