# Brushfire::PaymentMethodListOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**image_url** | **String** |  | [optional] 
**is_enabled** | **BOOLEAN** |  | [optional] 


