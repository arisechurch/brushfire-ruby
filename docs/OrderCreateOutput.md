# Brushfire::OrderCreateOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_key** | **String** |  | [optional] 
**order_id** | **String** |  | [optional] 
**object_number** | **Integer** |  | [optional] 
**order_total** | **Float** |  | [optional] 
**quantity** | **Integer** |  | [optional] 
**type** | **String** |  | [optional] 
**contact_email** | **String** |  | [optional] 
**product_numbers** | **Array&lt;Integer&gt;** |  | [optional] 


