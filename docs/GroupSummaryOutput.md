# Brushfire::GroupSummaryOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**group_number** | **Integer** |  | [optional] 
**name** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**attendee_type_id** | **String** |  | [optional] 
**type_name** | **String** |  | [optional] 
**attendee_count** | **Integer** |  | [optional] 
**admin_passcode** | **String** |  | [optional] 
**join_passcode** | **String** |  | [optional] 
**community_id** | **String** |  | [optional] 
**community_partition** | **String** |  | [optional] 
**community_name** | **String** |  | [optional] 


