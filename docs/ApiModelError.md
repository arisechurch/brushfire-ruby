# Brushfire::ApiModelError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errors** | [**Array&lt;KeyValuePairStringString&gt;**](KeyValuePairStringString.md) |  | [optional] 
**message** | **String** |  | [optional] 
**data** | **Object** |  | [optional] 


