# Brushfire::GroupCommunityInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**community_id** | **String** |  | [optional] 
**override_capacity** | **BOOLEAN** |  | [optional] 
**dont_follow_rules** | **BOOLEAN** |  | [optional] 


