# Brushfire::RegionsApi

All URIs are relative to *https://api.brushfire.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**regions_list**](RegionsApi.md#regions_list) | **GET** /regions | List all countries and regions with their Code for use in other calls


# **regions_list**
> Array&lt;RegionDetailOutput&gt; regions_list

List all countries and regions with their Code for use in other calls



### Example
```ruby
# load the gem
require 'brushfire'
# setup authorization
Brushfire.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Brushfire::RegionsApi.new

begin
  #List all countries and regions with their Code for use in other calls
  result = api_instance.regions_list
  p result
rescue Brushfire::ApiError => e
  puts "Exception when calling RegionsApi->regions_list: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Array&lt;RegionDetailOutput&gt;**](RegionDetailOutput.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml



