# Brushfire::PersonalInfoDataItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**number** | **Integer** |  | [optional] 
**first_name** | **String** |  | [optional] 
**last_name** | **String** |  | [optional] 
**phone** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**street1** | **String** |  | [optional] 
**street2** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**region** | **String** |  | [optional] 
**country** | **String** |  | [optional] 
**postal_code** | **String** |  | [optional] 
**address** | **String** |  | [optional] 
**custom1** | **String** |  | [optional] 
**has_name** | **BOOLEAN** |  | [optional] 


