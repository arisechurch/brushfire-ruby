# Brushfire::SessionCheckinEntryItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** |  | 
**checked_in_at** | **DateTime** |  | 


